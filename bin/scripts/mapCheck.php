#!/usr/bin/env apnscp_php
<?php

	use Daphnie\Chunker;
	use Opcenter\Account\Enumerate;
	use Opcenter\Apnscp;
	use Opcenter\Database\DatabaseCommon;
	use Opcenter\Database\PostgreSQL;
	use Opcenter\Database\PostgreSQL\Opcenter;
	use Opcenter\Filesystem;
	use Opcenter\Map;
	use function cli\get_instance;
	use function cli\parse;

	include(dirname(__FILE__, 3) . '/lib/CLI/cmd.php');

	$old = \Error_Reporter::set_verbose();
	\Error_Reporter::set_verbose(max($old, 2));

	$args = parse();
	$afi = get_instance();

	if (!$afi) {
		fatal("cannot init afi instance");
	}

	function tryImportSite(string $site, array $cfg): bool
	{
		try {
			return (new Opcenter(\PostgreSQL::pdo()))->createSite(
				$siteid = \Auth::get_site_id_from_anything($site),
				$cfg['domain'],
				$cfg['email'],
				$cfg['admin_user']
			);
		} catch (\PDOException $e) {
			if ((int)$e->getCode() !== 23505) {
				throw $e;
			}
			$domain = \Auth::get_domain_from_site_id($siteid);
			warn("Invalid domain record in siteinfo table: %(domain)s reported for %(siteid)d - deleting", [
				'domain' => $domain,
				'siteid' => $siteid
			]);
			(new Opcenter(\PostgreSQL::pdo()))->deleteSite($siteid, $domain);
			return tryImportSite($site, $cfg);
		}

	}
	/**
	 * Check
	 *
	 * @param bool $rebuild
	 */

	$command = array_get($args, 0, 'check');
	$rebuild = false;
	switch ($command) {
		case 'rebuild':
			$rebuild = true;
			break;
		case 'check':
			break;
		default:
			fatal("Unknown mode `%s'. 'rebuild', 'check' modes supported.", $command);
	}

	// physical directories requiring map checks
	$allSites = array_flip(array_map('basename', glob(FILESYSTEM_VIRTBASE . '/site[0-9]*', GLOB_ONLYDIR)));
	// sites present in domainmap but have no filesystem mapping

	$missingSites = array_diff_key(
		array_flip(array_values(Map::read(Map::DOMAIN_TXT_MAP)->fetchAll())),
		$allSites
	);

	$mockFilesystem = new class {
		use FilesystemPathTrait;

		public function set(string $site)
		{
			$this->site = $site;

			return $this;
		}

		public function get(string $service)
		{
			return $this->domain_info_path("current/${service}");
		}
	};
	$orphans = [];

	foreach (Map::META_MAPS as $map => $cfg) {
		info("Scanning %s", $map);
		if (!file_exists(Map::home($map))) {
			if (!$rebuild) {
				warn("Map %(map)s missing. %(rebuild)s.",
					[
						'map'     => $map,
						'rebuild' => sprintf(_("Run '%s rebuild' to resolve map inconsistencies"), $_SERVER['argv'][0])
					]
				);
				continue;
			}
			file_put_contents(\Opcenter\Map::home() . "/$map", '[DEFAULT]' . "\n");
		}
		if (!file_exists($map)) {
			touch(Map::home($map));
		}
		$mapper = Map::load($map, $rebuild ? 'cd' : 'r');

		foreach ($mapper->fetchAll() as $key => $site) {
			if (str_contains($site, ',') || str_contains($site, ' ')) {
				debug("%(map)s key %(key)s contains multiple owners - skipping: %(found)s", [
					'map' => $map,
					'key' => $key,
					'found' => implode(", ", (array)$site)
				]);
				continue;
			}
			if ($site !== \Opcenter\SiteConfiguration::RESERVED_SITE && !isset($allSites[$site])) {
				info("Invalid site reference %(site)s in %(map)s - tracking", ['site' => $site, 'map' => $map]);
				$missingSites[$site] = 1;
			}
			// look for maps which reference sites missing from /home/virtual
			if (isset($missingSites[$site])) {
				if ($rebuild) {
					info("Found dangling site %(site)s in %(map)s - removing", ['site' => $site, 'map' => $map]);
					$mapper->delete($key);
					if (str_ends_with($map, '.usermap')) {
						// locate database references
						$type = strtok($map, '.');
						$class = '\Opcenter\Database\\' . DatabaseCommon::canonicalizeBrand($type);
						if ($class::userExists($key)) {
							info("Detected database user %(user)s on %(type)s - removing", ['user' => $key, 'type' => $type]);
							$class::deleteUser($key);
						}
					}
				} else {
					info("Found dangling site %(site)s in %(map)s. %(rebuild)s.", [
						'site'    => $site,
						'map'     => $map,
						'rebuild' => sprintf(_("Run '%s rebuild' to resolve map inconsistencies"), $_SERVER['argv'][0])
					]);
				}
			}
		}

		[$service, $servicevar] = explode('.', $cfg);
		foreach (array_keys($allSites) as $site) {
			$ini = $mockFilesystem->set($site)->get($service);
			if (!file_exists($ini)) {
				if (!isset($orphans[$site])) {
					warn("%s MISSING %s - orphaned site?", $site, $service);
					$orphans[$site] = 1;
				}
				continue;
			}

			if (false === ($svcval = array_get(\Util_Conf::parse_ini($ini), $servicevar, false))) {
				fatal("EMERG failed to get %s from %s", $servicevar, $ini);
			}

			if ($map === 'domainmap') {
				$ini = $mockFilesystem->set($site)->get('aliases');
				if (!file_exists($ini)) {
					// *shrug*
					continue;
				}
				$aliases = \Util_Conf::parse_ini($ini);
				if (!array_key_exists('aliases', $aliases)) {
					fatal("EMERG failed to get %s from %s", $servicevar, $ini);
				}
				foreach ((array)$aliases['aliases'] as $alias) {
					if (!$mapper->exists($alias)) {
						if ($rebuild) {
							info("ADD %s=%s (%s - %s)", $alias, $site, $map, $servicevar);
							$mapper->set($alias, $site);
						} else {
							info("MISSING %s=%s (%s - %s)", $alias, $site, $map, $servicevar);
						}
					}
				}
			} else if ($map === 'billing.parentmap') {
				if (!$svcval) {
					// billing,parent_invoice isn't set
					continue;
				}
				if ($rebuild) {
					$mapper->close();
				}
				if (!($parent = \Auth::get_site_id_from_invoice($svcval))) {
					warn("%s MISSING PARENT INVOICE %s - orphaned site?", $site, $svcval);
					continue;
				}
				if ($rebuild) {
					$mapper = Map::load($map, 'cd');
				}
				$parent = ((array)$parent)[0];
				if ($mapper->exists($site) && $mapper[$site] === "site${parent}") {
					continue;
				}

				warn("%(site)s NOT IN %(map)s map - reports site%(parent)s as parent",
					['site' => $site, 'map' => $map, 'parent' => $parent]
				);
				if (!$rebuild) {
					continue;
				}
				info("ADD %s=%s (%s - %s)", $site, "site${parent}", $map, $servicevar);
				$mapper->set($site, "site${parent}");
				continue;
			} else if ($map === 'reseller') {
				if (!$svcval) {
					// ignore root resellers
					continue;
				}
			}

			foreach ((array)$svcval as $v) {
				if ($mapper->exists($v)) {
					continue;
				}
				warn("%(site)s NOT IN %(map)s map",
					['site' => $site, 'map' => $map]
				);
				if (!$rebuild) {
					continue;
				}
				info("ADD %s=%s (%s - %s)", $v, $site, $map, $servicevar);
				$mapper->set($v, $site);
			}

			$mapper->save();
		}
	}
	$mapper->close();

	//rebuild map
	Filesystem::readdir(FILESYSTEM_VIRTBASE, static function ($f) use ($rebuild) {
		$path = FILESYSTEM_VIRTBASE . "/${f}";
		if (is_link($path) && !file_exists($path)) {
			if (!$rebuild) {
				warn("Found dangling link %(path)s. %(rebuild)s.", [
					'path'    => $path,
					'rebuild' => sprintf(_("Run '%s rebuild' to resolve map inconsistencies"), $_SERVER['argv'][0])
				]);
			} else {
				info("Removing dangling link %s", $path);
				unlink($path);
			}
		}
	});


	$present = array_flip(
		array_values(
			array_intersect_key(
				$all = Map::load(Map::DOMAIN_TXT_MAP, 'r')->fetchAll(),
				array_flip($afi->admin_get_domains())
			)
		)
	);

	/**
	 * Populate missing siteinfo entries
	 */

	if ($rebuild) {
		info("Rebuilt %s", Map::DOMAIN_MAP);
		\Auth_Module::rebuildMap();
	} else {
		info("Run '%s rebuild' to resolve map inconsistencies", $_SERVER['argv'][0]);
		exit(!\Error_Reporter::is_error());
	}

	foreach (Enumerate::sites() as $site) {

		$ini = $mockFilesystem->set($site)->get('siteinfo');

		if (!file_exists($ini)) {
			if (!isset($orphans[$site])) {
				warn("%s MISSING %s - orphaned site?", $site, 'siteinfo');
			}
			continue;
		}

		$cfg = \Util_Conf::parse_ini($ini);

		$site_id = \Auth::get_site_id_from_anything($site);

		if (isset($present[$site]))
		{
			// consistency check
			if ("site$site_id" !== $site) {
				warn("Assertion failed. Wanted %(expected)s actual: %(actual)s",
					[
						'expected' => $site,
						'actual'   => "site$site_id"
					]
				);
				continue;
			}

			if (\Auth::get_domain_from_site_id($site_id) !== $cfg['domain']) {
				warn("updating %(record)s for `%(site)s' - old %(old)s -> %(new)s", [
					'record' => 'siteinfo.admin_user',
					'site' => $site,
					'old'  => \Auth::get_domain_from_site_id($site_id),
					'new'  => $cfg['domain']
				]);
				(new Opcenter(\PostgreSQL::pdo()))->changeDomain($site_id, $cfg['domain']);
			}
			if (\Auth::get_admin_from_site_id($site_id) !==  $cfg['admin_user']) {
				$oldAdmin = \Auth::get_admin_from_site_id($site_id);
				warn("updating %(record)s for `%(site)s' - old %(old)s -> %(new)s", [
					'record' => 'siteinfo.admin_user',
					'site' => $site,
					'old'  => $oldAdmin,
					'new'  => $cfg['admin_user']
				]);
				\PostgreSQL::pdo()->exec(PostgreSQL::vendor('user')->renameAdminUser($oldAdmin, $cfg['admin_user']));
			}

			continue;
		}

		if (tryImportSite($site, $cfg)) {
			info("Imported missing domain `%(domain)s' (`%(site)s')", [
				'domain' => $cfg['domain'],
				'site'   => \Auth::get_site_id_from_anything($site)
			]);
		} else {
			warn('failed to populate admin database entry for `%(domain)s\' (`%(site)s\')', [
				'domain' => $cfg['domain'],
				'site'   => \Auth::get_site_id_from_anything($site)
			]);
		}
	}

	$dbSitesInput = array_build((new Opcenter(\PostgreSQL::pdo()))->readSitesFromSiteinfo(), function ($key, $val) {
		return ["site$key", $val];
	});

	// verify /home/virtual/siteXX missing
	$dbSites = array_filter(array_diff_key($dbSitesInput, $present), static function (string $site) {
		$fst = new class($site) extends Filesystem {
			public function __invoke() {
				return $this->domain_fs_path();
			}
		};
		return !is_dir($fst());
	}, ARRAY_FILTER_USE_KEY);

	if ($dbSites) {
		$chunker = new Chunker(\PostgreSQL::pdo());
		info("Checking for metrics compression");
		$chunker->decompressAll();
	}

	foreach ($dbSites as $remove => $domain) {
		warn("Deleting `%(domain)s' (`%(site)s') from database", [
				'domain' => $domain,
				'site'   => $remove
		]);
		(new Opcenter(\PostgreSQL::pdo()))->deleteSite((int)substr($remove, 4), $domain);
	}

	// last sweep to verify /home/virtual/siteXX complete

	Apnscp::restart('now');
	exit(!\Error_Reporter::is_error());
