#!/bin/sh
PATH=/sbin:/usr/sbin:$PATH
sbd=`pidof shellinaboxd | awk {'print $1'}`
[[ "x$sbd" == "x" ]] && exit
ps --ppid $sbd -o pid,pgid | grep -v PID | while read p pgid; do
        if readlink /proc/$p/fd/0 | grep deleted; then
                kill -9 -$pgid
        fi
done
