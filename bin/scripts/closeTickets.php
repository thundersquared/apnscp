#!/usr/bin/env apnscp_php
<?php
/**
  * Usage: closetickets.php
  */
include(dirname(__FILE__).'/../../lib/CLI/cmd.php');

$age = 7*4; // 28 days 

$args = \cli\parse();
$afi  = \cli\get_instance();
if (!$afi) {
    fatal("cannot init afi instance");
}
$tickets = $afi->crm_get_trouble_tickets(1, 0, $age);
foreach ($tickets as $t) {
    info("Closing #%-3d - %s", $t['id'], $t['domain']);
    $afi->crm_close_trouble_ticket($t['id'], "");
}
