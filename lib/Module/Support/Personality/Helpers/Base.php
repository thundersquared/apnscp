<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Module\Support\Personality\Helpers;

	abstract class Base implements Contracts\Personality
	{
		// personality priority descending
		const PRIORITY = 1;
		// personality name
		const NAME = '';

		// ht directives
		protected $_tokens = array();
		// list of personalities that this active personality conflicts with
		protected $_conflictList = array();

		public function personalityName()
		{
			return static::NAME;
		}

		public function getPriority()
		{
			return static::PRIORITY;
		}

		/**
		 * Applied personality conflicts with another applied personality
		 *
		 * @return bool
		 */
		public function schizophrenic($personality)
		{
			return \in_array($personality, $this->_conflictList);
		}

		public function resolves($token)
		{

			return \array_key_exists($token, $this->_tokens);
		}

		public function test($directive, $val = null)
		{
			return null;
		}

		public function getDirectives()
		{
			return array_keys($this->_tokens);
		}

		public function getTokenDescription($directive)
		{
			if (!isset($this->_tokens[$directive])) {
				return '';
			}

			if (!isset($this->_tokens[$directive]['help'])) {
				return '';
			}

			return $this->_tokens[$directive]['help'];
		}

		/**
		 * Test a directive against 2 permitted options
		 *
		 * @param       $directive
		 * @param       $val
		 * @param array $accepted
		 * @return bool
		 */
		protected function _testFlag($directive, $val, $accepted = array('on', 'off'))
		{
			$valtest = strtolower($val);
			if (!\in_array($valtest, $accepted, true)) {
				return error("`%s' only accepts the value %s, `%s' given",
					$directive,
					join(' or ', $accepted),
					$val
				);
			}

			return true;
		}
	}
