<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, January 2021
 */

namespace Module\Support\Webapps;

use Opcenter\System\Cgroup\VolatileAttribute;
use Opcenter\System\Memory;

class ComposerWrapper extends PhpWrapper {

	/**
	 * Execute Composer command
	 *
	 * @param string|null $path
	 * @param string      $cmd
	 * @param array       $args
	 * @return array
	 */
	public function exec(?string $path, string $cmd, array $args = [], array $env = []): array
	{
		$maximalMemory = (int)($this->getMaximalMemory() * 0.85);

		if (WEBAPPS_COMPOSER_VOLATILE && !posix_getuid() && $this->cgroup_enabled()) {
			// remove memory lock
			$volatile = VolatileAttribute::instantiateContexted($this->getAuthContext());
			$attr = $volatile->fromAttribute('memory');
			$oldLimit = (int)$attr->read();
			$volatile->resource($attr);
			$maximalMemory = (int)(Memory::stats()['memtotal'] / 1024 * 0.85);
			debug("Suspending memory limit on %(site)s. Was %(limit)d MB",
				['site' => $this->getAuthContext()->site, 'limit' => $oldLimit / 1024 ** 2]
			);
		}
		$debug = is_debug() ? '-vvv ' : '-q ';
		$cmd = ' -d memory_limit=-1 /usr/bin/composer --no-interaction ' . $debug . $cmd;

		$ret = $this->direct($path, $cmd, $args, ['COMPOSER_MEMORY_LIMIT' => "${maximalMemory}m"] + $env);
		if (!$ret['success']) {
			error(
				"Failed executing Composer command `%(cmd)s': %(resp)s",
				['cmd' => $cmd, 'resp' => coalesce($ret['stderr'], $ret['stdout'])]
			);
		}

		$volatile = null;
		return $ret;
	}

	/**
	 * Execute command bypassing composer wrapper
	 *
	 * @param string $path
	 * @param string $cmd
	 * @param array  $args
	 * @param array  $env
	 * @return array
	 */
	public function direct(string $path = null, string $cmd, array $args = [], array $env = []): array
	{
		return parent::exec($path, $cmd, $args, $env);
	}
}