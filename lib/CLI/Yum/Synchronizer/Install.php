<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace CLI\Yum\Synchronizer;

	use CLI\Yum\Synchronizer;

	class Install extends Synchronizer
	{
		protected $service;
		protected $force = false;
		/**
		 * @var array optional dependencies
		 */
		protected $dependencies;

		/**
		 * Install constructor.
		 *
		 * @param array ...$args
		 *
		 * Signature 1: <flags>, $package, $service
		 * Signature 2: $package, $service
		 * Signature 3: $package, $service, $version, $rel
		 */
		public function __construct(...$args)
		{
			foreach (\func_get_args() as $a) {
				if ($a[0] === '-') {
					switch ($a) {
						case '--force':
							$this->force = true;
							break;
						case '-d':
							$this->dependencies = [];
							break;
						default:
							fatal("Unrecognized flag `%s'", $a);
					}
					array_shift($args);
				}
			}
			$service = array_pop($args);
			$this->package = $args;
			$this->service = $service;

			if (!$service) {
				fatal('Missing service, usage: install <package> <service>');
			}
			$svcpath = Utils::getServicePath($service);
			if (!is_dir($svcpath)) {
				fatal("service path `%s' does not exist", $svcpath);
			}
		}

		protected function installDependencies()
		{
			$depmap = new DependencyMap($this->package);
			if (!$depmap->resolve()) {
				fatal("Failed to resolve dependencies for `%s'", $this->package);
			}
			foreach ($depmap->getWants() as $dep) {
				if (isset($this->dependencies[$dep])) {
					continue;
				}
				$this->dependencies[$dep] = true;
				info("Adding dependency `%s' to be installed under `%s'", $dep, $this->service);
				if (!(new Install($dep, $this->service))->run()) {
					fatal("Failed to install dependency `%s' in `%s'", $dep, $this->service);
				}
			}

			return true;
		}

		public function run()
		{
			$svc = $this->service;
			$ret = true;
			$allFiles = Utils::getFilesFromRPM(...$this->package);
			$hasMultiple = \count($this->package) > 1;
			if (false === $allFiles && !$hasMultiple) {
				return false;
			}
			foreach ($this->package as $pkg) {

				if (null !== ($tmp = Utils::getServiceFromPackage($pkg))) {
					if (!$this->force || $tmp !== $this->service) {
						$macro = $tmp === $this->service ? 'warn' : 'error';
						return $macro("package `%s' already installed under `%s'", $pkg, $tmp);
					}
				}
				if (null !== $this->dependencies) {
					$this->installDependencies();
				}

				$files = $allFiles;
				if ($hasMultiple) {
					// package not installed
					if (!isset($allFiles[$pkg])) {
						continue;
					}
					$files = $allFiles[$pkg];
				}

				$svcpath = Utils::getServicePath($svc);
				foreach ($files as $f) {
					$this->installFile($f, $svcpath);
				}
				$meta = Utils::getMetaFromRPM($pkg);

				$ret &= $this->postTransaction($pkg, $svc, $meta['version'], $meta['release']);
			}
			return (bool)$ret;
		}

		public function postTransaction($package, $service, $version, $release)
		{
			$db = \PostgreSQL::initialize();
			$db->query('SELECT service FROM site_packages where package_name = ' . pg_escape_literal($db->getHandler(), $package));
			$exists = $db->num_rows() > 0;
			if ($exists) {
				$db->query("UPDATE site_packages SET deleted = 'f', version = " . pg_escape_literal($db->getHandler(), $version) . ', release = ' .
					pg_escape_literal($release) . ' WHERE package_name = ' . pg_escape_literal($db->getHandler(), $package));
				info("Reinstalled `%s' under `%s'", $package, $service);
			} else {
				$db->query("INSERT INTO site_packages (package_name, version, service, release) VALUES('" .
					pg_escape_string($db->getHandler(), $package) . "', '" . pg_escape_string($db->getHandler(), $version) . "', '" .
					pg_escape_string($db->getHandler(), $service) . "', '" . pg_escape_string($db->getHandler(), $release) . "')");
			}
			SynchronizerCache::get()->addPackage((new Synchronizer\CacheManager\Package(
				$package,
				$version,
				$release
			))->addFiles(Utils::getFilesFromRPM($package)));
			Synchronizer\Plugins\Manager::run($package, 'install');

			return $db->affected_rows() > 0;
		}

	}
