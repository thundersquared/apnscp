<?php declare(strict_types=1);
/*
 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * 	Unauthorized copying of this file, via any medium, is
 * 	strictly prohibited without consent. Any dissemination of
 * 	material herein is prohibited.
 *
 * 	For licensing inquiries email <licensing@apisnetworks.com>
 *
 * 	Written by Matt Saladna <matt@apisnetworks.com>, December 2022
 */

	namespace Opcenter\Admin\Settings\Pgsql;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Versioning;

	class PostgisVersion implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val == $this->get()) {
				//return true;
			}

			$cfg = new Bootstrapper\Config();
			$cfg['pgsql_has_postgis'] = (bool)$val;
			if ($val) {
				$cfg['postgis_version'] = Versioning::valid((string)$val) ? $val : null;
			}

			return $cfg->run('pgsql/install');
		}

		public function get()
		{
			$cfg = new Bootstrapper\Config();

			return $cfg['pgsql_has_postgis'] ? $cfg['postgis_version'] : false;
		}

		public function getHelp(): string
		{
			return 'Enable PostGIS support';
		}

		public function getValues()
		{
			return 'bool|string';
		}

		public function getDefault()
		{
			return false;
		}

	}
