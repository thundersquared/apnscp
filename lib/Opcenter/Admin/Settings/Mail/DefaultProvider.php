<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\Mail;

	use Opcenter\Admin\Settings\Cp\Bootstrapper;
	use Opcenter\Admin\Settings\Cp\Config;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Apnscp;
	use Opcenter\Mail;

	class DefaultProvider implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val === null) {
				$val = 'null';
			}
			if (!\Opcenter\Mail::providerValid($val)) {
				return error("Invalid mail provider `%s'", $val);
			}

			$cfg = new Config();
			$cfg->setRestart(false);
			if ($val !== ($old = $cfg->get('mail', 'provider_default')) &&
				'' != ($oldkey = $cfg->get('mail', 'provider_key'))) {
				warn("Mail provider changed from `%s' to `%s' - cleared provider key `%s'",
					$old, $val, $oldkey
				);
				$cfg->set('mail', 'provider_key', null);
			}

			return $cfg->set('mail', 'provider_default', $val) && (new Bootstrapper())->set('mail_default_provider', $val)
				&& Apnscp::restart();
		}

		public function get()
		{
			return (new Config())->get('mail', 'provider_default');
		}

		public function getHelp(): string
		{
			return 'Default mail provider assigned to accounts';
		}

		public function getValues()
		{
			return Mail::providers();
		}

		public function getDefault()
		{
			return 'builtin';
		}

	}
