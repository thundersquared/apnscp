<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\Mail;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;

	class RspamdPiggyback implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val == $this->get()) {
				// no need to set the value again
				return true;
			}

			$config = new Config();
			$config['rspamd_enabled'] = (bool)$val;

			if ((new SpamFilter())->get() === 'rspamd') {
				warn("Spam filter configured as rspamd. Ignoring piggyback");
				return true;
			}

			if (!$config['mail_enabled']) {
				return error("Mail service disabled");
			}

			unset($config);
			Bootstrapper::run('mail/rspamd', 'software/argos');
			return true;
		}

		public function get()
		{
			$cfg = new Config();
			return $cfg['rspamd_enabled'] && (new SpamFilter())->get() === 'spamassassin';
		}

		public function getHelp(): string
		{
			return 'Enable rspamd for passive learning from SpamAssassin';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return false;
		}

	}
