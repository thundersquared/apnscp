<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, October 2019
 */

namespace Opcenter;

class Blacklist {

	// @var array $blacklist
	private $blacklist;

	public function __construct(?array $blacklist = [])
	{
		$this->blacklist = (array)$blacklist;
	}

	/**
	 * Filter exclusion list
	 *
	 * @param array $items
	 * @return array
	 */
	public function filter(array $items): array
	{
		$keep = array_flip($items);
		foreach ($this->blacklist as $bl) {
			if ($bl === '*') {
				$keep = [];
			}
			if ($bl[0] !== '!') {
				if (false === strpos($bl, '*')) {
					unset($keep[$bl]);
				} else {
					// wildcard
					foreach (array_keys($keep) as $chk) {
						if (fnmatch($bl, $chk)) {
							unset($keep[$chk]);
						}
					}
				}
			} else {
				$wl = substr($bl, 1);
				if (false === strpos($wl, '*')) {
					if (!isset($keep[$wl])) {
						$keep[$wl] = 1;
					}
				} else {
					foreach ($items as $chk) {
						if (fnmatch($wl, $chk)) {
							$keep[$chk] = 1;
						}
					}
				}

			}
		}

		return array_keys($keep);
	}
}