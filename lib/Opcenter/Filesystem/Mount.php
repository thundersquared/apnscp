<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	namespace Opcenter\Filesystem;

	class Mount
	{
		const MTAB_PATH = '/proc/self/mounts';

		protected static function readMountInfo(): string
		{
			if (!file_exists($path = '/proc/self/mountinfo')) {
				$path = '/proc/' . posix_getpid() . '/mountinfo';
			}
			return file_get_contents($path);
		}

		protected static function readMounts(): string
		{
			if (!file_exists($path = self::MTAB_PATH)) {
				$path =  '/proc/' . posix_getpid() . '/' . basename(self::MTAB_PATH);
			}

			return file_get_contents($path);
		}

		/**
		 * Mount a filesystem
		 *
		 * @param string $path
		 * @param string $device
		 * @param array  $options -o options
		 * @param string $flags
		 * @return bool
		 */
		public static function mount($path, $device, array $options = [], array $flags = []): bool
		{
			$proc = new \Util_Process();
			// avoid polluting /etc/mtab
			$proc->setEnvironment('LIBMOUNT_MTAB', static::MTAB_PATH);
			$opts = array_key_map(static function ($k, $v) {
				if (!$v) {
					return ['-O', $k];
				}
				if ($v === true || $v === 1) {
					return ['-o', $k];
				}

				return ['-o', "${k}=" . (string)$v];
			}, $options);
			$ret = $proc->run(array_flatten(['mount', '-n', '-t', '%(device)s', $opts, $flags,  '%(device)s', '%(path)s']), [
				'device' => $device,
				'path'   => $path,
			]);

			return $ret['success'] ?: error("failed to mount `%s': %s", $path, $ret['stderr']);
		}

		/**
		 * Unmount path
		 *
		 * @param string      $path
		 * @param string|null $flags optional flags to pass to umount
		 * @return bool
		 */
		public static function unmount(string $path, string $flags = null): bool
		{
			$proc = new \Util_Process();
			$proc->setEnvironment('LIBMOUNT_MTAB', static::MTAB_PATH);
			$ret = $proc->run(['umount', '-f', '%(flags)s', '%(path)s'], ['path' => $path, 'flags' => $flags]);

			return $ret['success'] ?: error("failed to unmount path `%s': `%s'", $path, $ret['stderr']);
		}

		/**
		 * Get mount-point for file or directory
		 *
		 * @param string $path
		 * @return null|string
		 */
		public static function getMount(string $path): ?string
		{
			if (null === ($dev = self::getDeviceFromPath($path))) {
				return null;
			}
			$str = sprintf('%u:%u', $dev[0], $dev[1]);
			$mountinfo = self::readMountInfo();
			if (false === ($pos = strpos($mountinfo, $str))) {
				warn('mount for device %u:%u not present in /proc/self/mountinfo', $dev[0], $dev[1]);

				// mount not present in mountinfo?
				return null;
			}
			// major:minor device
			strtok(substr($mountinfo, $pos), ' ');
			// root of the mount within the filesystem
			strtok(' ');

			// mount point
			return strtok(' ');
		}

		/**
		 * Get device from path
		 *
		 * @param string $path
		 * @return array|null
		 */
		public static function getDeviceFromPath(string $path): ?array
		{
			if (!file_exists($path) && !is_link($path)) {
				return null;
			}
			if (false === ($stat = stat($path))) {
				warn("unable to determine mount, failed to stat path `%s'", $path);

				return null;
			}

			return self::extractDeviceMajorMinor($stat['dev']);
		}

		private static function extractDeviceMajorMinor(int $devid): array
		{
			$major = (($devid >> 8) & 0xFFF) | (($devid >> 32) & ~0xFFF);
			return [$major, ($devid & 0xFF) | (( $devid >> 12) & ~0xFF)];
		}

		/**
		 * Get major:minor from special device
		 *
		 * @param string $special
		 * @return array|null
		 */
		public static function getDeviceId(string $special): ?array
		{
			if (!file_exists($special)) {
				return null;
			}
			if (false === ($stat = stat($special))) {
				warn("failed to stat path `%s'", $special);
				return null;
			}
			if (0 === ($stat['mode'] & (0020000 | 0060000))) {
				// not a special device
				return null;
			}

			return self::extractDeviceMajorMinor($stat['rdev']);
		}

		/**
		 * Get mount point from device
		 *
		 * @param string $device
		 * @return string|null
		 */
		public static function getMountPointFromDevice(string $device): ?string
		{
			static $known = [];
			if (isset($known[$device])) {
				return $known[$device];
			}

			if (false === strpos($device, ':') || strcspn($device, '0123456789:')) {
				fatal("Device `%s' not listed as major:minor", $device);
			}

			$ret = \Util_Process::exec(["findmnt", $device, "-fn"]);
			return $known[$device] = ($ret['success'] ? strtok($ret['stdout'], ' ') : null);
		}

		/**
		 * Get device mount options
		 *
		 * @param string $mount
		 * @return array|null
		 */
		public static function getMountOptions(string $mount): ?array
		{
			if (null === ($dev = self::getDeviceFromPath($mount))) {
				return null;
			}
			$str = sprintf('%x:%x', $dev[0], $dev[1]);
			$mountinfo = self::readMountInfo();
			if (false === ($pos = strpos($mountinfo, $mount)) &&
				false === ($pos = strpos($mountinfo, $str))) /* $mount is path below mount point */
			{
				warn('mount for device %x:%x not present in /proc/self/mountinfo', $dev[0], $dev[1]);

				// mount not present in mountinfo?
				return null;
			}

			// line
			$line = strtok(substr($mountinfo, $pos), "\n");

			// mount options
			return explode(',', substr($line, strrpos($line, ' ')+1));
		}

		/**
		 * Get filesystem type for mount point
		 *
		 * @param string $mount
		 * @return string|null
		 */
		public static function getFilesystemType(string $mount): ?string
		{
			if (null === ($dev = self::getDeviceFromPath($mount))) {
				return null;
			}
			$str = sprintf('%d:%d', $dev[0], $dev[1]);
			$mountinfo = self::readMountInfo();
			if (false === ($pos = strpos($mountinfo, $str)) ||
				false === ($pos = strpos($mountinfo, $mount . ' ', $pos))) /* $mount is path below mount point */ {
				warn('mount for device %x:%x not present in /proc/self/mountinfo', $dev[0], $dev[1]);

				// mount not present in mountinfo?
				return null;
			}

			// line
			$line = substr($mountinfo, $pos);
			// check if first field is maj:min
			if (false === strpos(substr($line, 0, strpos($line, ' ')), ':')) {
				// rewind to start
				$line = substr($mountinfo, $pos - strlen($str)-1);
			}
			$line = strtok($line, "\n");

			// mount options
			return explode(' ', $line)[6];
		}


		/**
		 * Get device underlying block device
		 *
		 * @param string $device
		 * @return string|null
		 */
		public static function getBlockFromDevice(string $device): ?string
		{
			if (false === strpos($device, ':') || strcspn($device, '0123456789:')) {
				fatal("Device `%s' not listed as major:minor", $device);
			}
			if (!is_link('/sys/dev/block/' . $device)) {
				return null;
			}

			$link = readlink('/sys/dev/block/' . $device);
			while ($link) {
				$dir = \dirname($link);
				$blockdev = basename($link);
				if (file_exists('/sys/block/' . $blockdev)) {
					return $blockdev;
				}
				$link = $dir;
			}
			return null;
		}

		/**
		 * Get block device from path
		 *
		 * @param string $path
		 * @return string|null
		 */
		public static function getBlockFromPath(string $path): ?string
		{
			if (null === ($device = static::getDeviceFromPath($path))) {
				return null;
			}

			return static::getBlockFromDevice($device[0] . ':' . $device[1]);
		}

		/**
		 * Get IO elevator scheduler
		 *
		 * @param string $block
		 * @return string|null
		 */
		public static function getScheduler(string $block): ?string
		{
			if (0 === strncmp($block, '/dev/', 5)) {
				$block = static::getBlockFromDevice($block);
			}
			if (!file_exists($path = '/sys/block/' . $block . '/queue/scheduler')) {
				// passed as sda1, translate to sda
				if (file_exists('/dev/' . $block)) {
					if (! ($majmin = implode(':', (array)static::getDeviceId('/dev/' . $block))) ) {
						return null;
					}
					$parent = static::getBlockFromDevice($majmin);
					debug('Converted %s to %s (%s)', $block, $parent, $majmin);

					return static::getScheduler($parent);
				}
				return null;
			}
			$scheduler = file_get_contents($path);
			if (false === ($str = strtok($scheduler, ']'))) {
				return null;
			}

			return strtok(strtok($str, '['), ' ');
		}

		/**
		 * Paths share a common mount-point
		 *
		 * @param string $p1 path
		 * @param string $p2 path
		 * @return bool
		 */
		public static function sameMount(string $p1, string $p2): bool
		{
			$tmp = self::getDeviceFromPath($p1);

			return null !== $tmp && $tmp === self::getDeviceFromPath($p2);
		}

		/**
		 * Path is mounted
		 *
		 * @param string $path
		 * @return bool
		 */
		public static function mounted(string $path): bool
		{
			return preg_match('!^\S+\s' . preg_quote($path, '!') . '\s!m', self::readMounts()) > 0;
		}
	}
