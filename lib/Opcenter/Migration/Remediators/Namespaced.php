<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2019
	 */

	namespace Opcenter\Migration\Remediators;

	use Opcenter\Migration\Remediator;

	class Namespaced extends Remediator
	{
		// cap namespacing
		const MAXLEN = \User_Module::USER_MAXLEN;
		/**
		 * Item conflicts
		 *
		 * @param string $key
		 * @param        $val
		 * @return bool
		 */
		public function conflict(string $key, $val): bool
		{
			$hostname = $val['hostname'] ?? $val;
			// let primary domain always win
			$collisionResolver = $this->createResolverFragment($hostname);
			$isPrimary = $this->getAuthContext()->domain === $hostname;
			if (!$isPrimary && false !== strpos($key, '-' . $collisionResolver)) {
				return true;
			}
			return isset($this->data[$key]);
		}

		public function resolve(string &$key, &$val): bool
		{
			$hostname = $val['hostname'] ?? $val;
			$tmp = $key;
			// case where domains example.com, example.org, example.net exist
			$append = $this->createResolverFragment($hostname);
			$key .= '-' . $append;
			if (\strlen($key) > self::MAXLEN) {
				$key = substr($key, 0, self::MAXLEN);

			}
			if (isset($this->data[$key])) {
				// conflict, append hostname/val to resolve up to maxlen
				$new = substr($key, 0, \strlen($tmp));
				$suffix = str_replace('.', '-', $append);
				$len = (int)min(0, self::MAXLEN - \strlen($new) - \strlen($suffix) - 1 /* dash */);
				$key = $new . '-' . substr($suffix, -$len);
				if (\strlen($key) > self::MAXLEN) {
					$key = substr($key, 0, self::MAXLEN);
				}
			}

			if (isset($this->data[$key])) {
				// one last try with the full domain, multiple email@basedomains exist
				$key = substr($key, 0, -\strlen($append)) . $hostname;
				if (isset($this->data[$key])) {
					return false;
				}
			}

			info('Namespaced remediation strategy: converted %s to %s', $tmp, $key);
			return true;
		}

		/**
		 * Truncate TLD component from hostname
		 *
		 * @param string $val
		 * @return string
		 */
		private function createResolverFragment(string $val): string {
			return substr($val, 0, strpos($val, '.') ?: \strlen($val));
		}
	}
