<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace Opcenter\Net\Firewall;

	class Rule implements \JsonSerializable
	{
		protected $host;
		protected $group;

		use \PropertyMutatorAccessorTrait;

		public function __construct(array $items = [])
		{
			foreach ($items as $k => $v) {
				$this->__set($k, $v);
			}
		}

		public function getHost(): string
		{
			return $this->host;
		}

		public function getGroup(): string
		{
			return $this->group;
		}

		public function getRecord(): string
		{
			return $this->host;
		}

		public function isBlocked(): bool
		{
			return true;
		}

		#[\ReturnTypeWillChange]
		public function jsonSerialize()
		{
			return get_object_vars($this);
		}


	}

