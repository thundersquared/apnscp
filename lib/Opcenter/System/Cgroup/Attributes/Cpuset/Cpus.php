<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2018
	 */


	namespace Opcenter\System\Cgroup\Attributes\Cpuset;

	use Opcenter\System\Cgroup\Attributes\BaseAttribute;
	use Opcenter\System\Cgroup\Controller;

	class Cpus extends BaseAttribute
	{
		public function __construct($value, Controller $controller)
		{
			$this->controller = $controller;
			$this->value = $value;
			if (null === $value) {
				return;
			}

			$this->value = (array)$this->value;
			foreach ($this->value as &$val) {
				$val = (int)$val % NPROC;
			}
			unset($val);
		}

		private function expandRange(string $val): array
		{
			return array_map('intval', range(...explode('-', $val)));
		}

		public function read()
		{
			$val = parent::read();
			if (null === $val || "" === $val) {
				return null;
			}
			$cpus = [];
			foreach (explode(',', $val) as $cpu) {
				if (false === strpos($val, '-')) {
					$cpus[] = (int)$cpu;
				} else {
					$cpus = array_merge($cpus, $this->expandRange($cpu));
				}
			}

			return $cpus;
		}

		public function getValue()
		{
			$val = parent::getValue();
			return $val === null ? $val :
				implode(',', (array)$val);
		}

		public function activate(): bool
		{
			$path = $this->getAttributePath();
			$value = $this->getValue();
			return $this->write($path, $value);
		}

		public function deactivate(): bool
		{
			$path = $this->getAttributePath();

			return file_put_contents($path, "0-" . (NPROC - 1)) > 0;
		}
	}