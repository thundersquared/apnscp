<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, August 2019
 */

namespace Opcenter\Http\Php\Fpm;

use Opcenter\SiteConfiguration;
use Opcenter\System\Cgroup;

class ResourceManager {
	private const SYSTEMD_ROOT_SLICE = 'system.slice';

	// @var group
	protected Cgroup\Group $group;
	/**
	 * @var SiteConfiguration
	 */
	protected SiteConfiguration $svc;

	public function __construct(SiteConfiguration $svc) {
		$this->group = new Cgroup\Group(self::SYSTEMD_ROOT_SLICE);
		$this->svc = $svc;
	}

	/**
	 * Get group name
	 *
	 * @return string
	 */
	public function getGroup(): string {
		return (string)$this->group;
	}

	public function setGroup(string $name) {
		$group = new Cgroup\Group($name);
		$firstController = Cgroup::getControllers()[0];
		if (!Cgroup::exists(Cgroup\Controller::make($group, $firstController))) {
			warn("Named group `%s' does not exist", $name);
		}
		$this->group = $group;
	}

	public function resourceControllers(): array {
		$controllers = [];
		foreach (Cgroup::getControllers() as $controllerName) {
			$controller = Cgroup\Controller::make($this->group, $controllerName);
			if ($controller->exists() && $controller->immmediateBinding()) {
				$controllers[] = $controllerName;
			}

		}
		return $controllers;
	}
}