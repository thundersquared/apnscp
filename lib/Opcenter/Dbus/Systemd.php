<?php declare(strict_types=1);
/*
 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * 	Unauthorized copying of this file, via any medium, is
 * 	strictly prohibited without consent. Any dissemination of
 * 	material herein is prohibited.
 *
 * 	For licensing inquiries email <licensing@apisnetworks.com>
 *
 * 	Written by Matt Saladna <matt@apisnetworks.com>, December 2022
 */

namespace Opcenter\Dbus;

class Systemd
{
	private const ESCAPE_PAIRS = [
		'_40' => '@',
		'_2e' => '.',
		'_5f' => '_',
		'_2d' => '-',
		'_5c' => '\\'
	];
	protected const CONNECTION_NAME = 'org.freedesktop.systemd1';

	/**
	 * Convert a systemd unit into a dbus object path
	 * @param string $unit
	 * @return string|null null on missing unit
	 */
	private function unitToObject(string $unit): ?string
	{
		$unit = $this->qualifyUnitName($unit);
		if (false !== strpos($unit, '*')) {
			warn("Wildcard not supported - unit `%s'", $unit);
			report("Wildcard unit `%s'", $unit);
			return null;
		}
		$cache = \Cache_Global::spawn();
		if (false !== ($obj = $cache->get($unit))) {
			return $obj;
		}

		try {
			$obj = $this->proxy('org.freedesktop.systemd1.Manager')->LoadUnit($unit)->getData();
		} catch (\DbusException $e) {
			error($e->getMessage());
			return null;
		}
		$cache->set($unit, $obj, 300);
		return $obj ?: null;
	}

	/**
	 * Named unit is qualified
	 *
	 * @param string $unit
	 * @return string
	 */
	private function qualifyUnitName(string $unit): string
	{
		$ext = false === ($pos = strrpos($unit, '.')) ? $unit : substr($unit, ++$pos);
		if ($ext !== 'service' && $ext !== 'socket' && $ext !== 'timer' && $ext !== 'mount' && $ext !== 'slice') {
			$unit = "{$unit}.service";
		}

		return $unit;
	}

	/**
	 * Unit exists in systemd
	 *
	 * Does not report activation of service but availability
	 *
	 * @param string $unit
	 * @return bool
	 */
	public function exists(string $unit): bool
	{
		// LoadUnit will always return an object representation of the unit, present or missing
		$object = $this->unitToObject($unit, true);
		try {
			$service = $this->pathToName(basename($object));
			// GetUnit throws exception if service isn't enabled
			$this->exec(null, 'Manager')->GetUnitFileState($service);
		} catch (\DBusException $e) {
			return false;
		}
		return true;
	}

	/**
	 * Unescape D-Bus path to name
	 *
	 * _2e -> ., _2d -> -, etc
	 *
	 * @param string $path
	 * @return string
	 */
	private function pathToName(string $path): string
	{
		return str_replace(array_keys(self::ESCAPE_PAIRS), array_values(self::ESCAPE_PAIRS), $path);
	}

	/**
	 * Convert D-Bus unit name to path
	 *
	 * . -> _2e, - -> 2d, etc
	 * @param string $name
	 * @return string
	 */
	private function nameToPath(string $name): string
	{
		return str_replace(array_values(self::ESCAPE_PAIRS), array_keys(self::ESCAPE_PAIRS), $name);
	}

	/**
	 * Read unit field
	 *
	 * @param string $unit  unit name
	 * @param string $field field
	 * @return mixed
	 */
	public function read(string $unit, string $field, string $object = 'Service')
	{
		if (null === ($resolvedUnit = $this->unitToObject($unit))) {
			return null;
		}

		return (new DataWrapper($this->proxy('org.freedesktop.DBus.Properties', $resolvedUnit)))->Get(
			'org.freedesktop.systemd1.' . ucfirst($object), $field);
	}

	/**
	 * Read interface properties
	 *
	 * @param string $unit
	 * @param string $object
	 * @return array|null
	 */
	public function readAllProperties(string $unit, string $object = 'Service'): ?array
	{
		if (null === ($resolvedUnit = $this->unitToObject($unit))) {
			return null;
		}
		try {
			return (new DataWrapper($this->proxy('org.freedesktop.DBus.Properties',
				$resolvedUnit)))->GetAll('org.freedesktop.systemd1.' . ucfirst($object));
		} catch (\DbusException $e) {
			error('Failed to query %s: %s', $unit, $e->getMessage());
			return [];
		}
	}

	/**
	 * Wait for job execution
	 *
	 * @param int $id
	 * @return bool
	 */
	public function jobWait(int $id, string $object = null): void
	{

		$object = $object ?? "/org/freedesktop/systemd1/job/{$id}";
		try {
			while (true) {
				$proxy = $this->proxy('org.freedesktop.DBus.Properties', $object);
				$proxy->Get('org.freedesktop.systemd1.Job', 'State');
				usleep(10000);
			}
		} catch (\DbusException $e) {
			return;
		}
	}

	/**
	 * Read unit field
	 *
	 * @param string $unit  unit name
	 * @param string $field field
	 * @return mixed
	 * @throws \DbusException
	 */
	public function exec(?string $unit, string $object = 'Service')
	{
		$resolvedUnit = null;

		if ($unit && null === ($resolvedUnit = $this->unitToObject($unit))) {
			throw new \DbusException("Service {$unit} does not exist");
		}
		$bus = $this->proxy('org.freedesktop.systemd1.' . ucfirst($object), $resolvedUnit);
		return new DataWrapper($bus);
	}

	/**
	 * Invoke systemctl daemon-reload
	 *
	 * @return void
	 */
	public function reload(): void
	{
		$this->exec(null, 'Manager')->Reload();
	}

	/**
	 * Invoke systemctl daemon-reexec
	 *
	 * @return void
	 */
	public function restart(): void
	{
		$this->exec(null, 'Manager')->Reexecute();
	}

	/**
	 * @param string $interface DBus interface
	 * @param string $object    DBus object path
	 * @return \DBusObject
	 */
	protected function proxy(string $interface, string $object = null): \DBusObject
	{
		if (!$object) {
			$object = '/' . str_replace('.', '/', static::CONNECTION_NAME);
		}

		return (new \Dbus(\Dbus::BUS_SYSTEM))->createProxy(static::CONNECTION_NAME, $object, $interface);
	}

}

