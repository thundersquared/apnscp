<?php declare(strict_types=1);
/*
 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * 	Unauthorized copying of this file, via any medium, is
 * 	strictly prohibited without consent. Any dissemination of
 * 	material herein is prohibited.
 *
 * 	For licensing inquiries email <licensing@apisnetworks.com>
 *
 * 	Written by Matt Saladna <matt@apisnetworks.com>, December 2022
 */

namespace Opcenter\Dbus;

class DataWrapper {
	protected \DbusObject $bus;

	public function __construct(\DbusObject $bus)
	{
		$this->bus = $bus;
	}

	public function __call($method, $args) {
		$resp = $this->bus->$method(...$args);
		return $this->convert($resp);
	}

	private function convert($v)
	{
		if ($v instanceof \DbusVariant) {
			$v = $v->getData();
		}
		if ($v instanceof \DbusStruct || $v instanceof \DbusDict || $v instanceof \DbusArray || $v instanceof \DbusObjectPath) {
			$v = $v->getData();
		}
		if (is_array($v)) {
			return array_map([$this, 'convert'], $v);
		}

		return $v;
	}

	public function raw($method, ...$args) {
		return $this->bus->$method(...$args);
	}
}
