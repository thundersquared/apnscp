<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Diskquota;

	use Opcenter\Service\ServiceValidator;

	class Amnesty extends ServiceValidator
	{
		const DESCRIPTION = 'Last storage amnesty request in unixtime';
		const VALUE_RANGE = '[null,0-∞]';

		public function valid(&$value): bool
		{
			return null === $value || (is_numeric($value) && $value > 0);
		}
	}