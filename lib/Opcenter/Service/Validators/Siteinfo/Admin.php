<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Siteinfo;

	use Opcenter\Service\ServiceValidator;

	class Admin extends ServiceValidator
	{
		const DESCRIPTION = 'Non-virtual user mapping to system (adminXX)';

		public function valid(&$value): bool
		{
			if (!$this->ctx->hasOld()) {
				if (!$value) {
					$value = \Opcenter\Role\User::gen_admin();
				}
				if (0 !== strncmp($value, 'admin', 5) || !ctype_digit(substr($value, 5))) {
					return error('admin must follow adminXX format');
				}
				if (posix_getpwnam($value)) {
					return error("user `%s' already in use on server - inconsistent state?", $value);
				} else if (posix_getgrnam($value)) {
					return error("group `%s' already in use on server - inconsistent state?", $value);
				}
			} else if ($this->ctx->isEdit() && $this->ctx->getNewServiceValue(null,
					'admin') !== $this->ctx->getOldServiceValue(null, 'admin')) {
				return error('cannot change internal admin mapping');
			}

			return true;
		}

	}

