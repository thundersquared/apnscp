<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Reseller;

	use Opcenter\SiteConfiguration;

	class ParentId extends ResellerId
	{
		const MAP_FILE = 'reseller.map';
		const DESCRIPTION = 'Reseller parent ID declaration';

		public function valid(&$value): bool
		{
			if (!$value) {
				return true;
			}

			if (!parent::valid($value)) {
				return false;
			}

			if (!$this->mapKeyInUse($value, static::MAP_FILE, $value)) {
				return error("Parent reseller ID %d not on server", $value);
			}

			return true;
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			return $old === $new;
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return true;
		}


	}