<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service;

	use Event\Contracts\Publisher;
	use Event\Contracts\Subscriber;
	use Event\Events;
	use Opcenter\Map;
	use Opcenter\Service\Contracts\AlwaysRun;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\Contracts\Validator;
	use Opcenter\SiteConfiguration;

	abstract class ServiceValidator implements Validator, Subscriber
	{
		use \NamespaceUtilitiesTrait;
		// validator description for help
		const DESCRIPTION = '<no help available>';
		// arbitrary value hint for help, no other purpose
		const VALUE_RANGE = '<string>';

		/**
		 * @var ConfigurationContext
		 */
		protected $ctx;
		/**
		 * @var string site
		 */
		protected $site;

		protected $force = false;

		public function __construct(ConfigurationContext $ctx, ?string $site)
		{
			$this->ctx = $ctx;
			$this->site = $site;
		}

		public function update($event, Publisher $caller)
		{
			return $this->fire($event, $caller);
		}

		/**
		 * Fire event handlers
		 *
		 * @param                   $event
		 * @param SiteConfiguration $caller
		 * @return bool
		 */
		public function fire($event, SiteConfiguration $caller): bool
		{
			if ((!$this instanceof ServiceInstall && !$this instanceof AlwaysRun) || !$this->hasChange()) {
				return true;
			}

			$enabled = $this->ctx->getServiceValue(null, 'enabled');
			$event = explode('.', $event, 2);
			switch (array_pop($event)) {
				case Events::SUCCESS:
					if (($enabled || $this instanceof AlwaysRun) && ($ret = $this->populate($caller))) {
						//Cardinal::preempt([Service::HOOK_ID, Events::FAILURE], $this);
						return $ret;
					}
				case Events::FAILURE:
					if ($enabled || $this instanceof AlwaysRun) {
						return $this->depopulate($caller);
					}

					return true;
					break;
				default:
					fatal("unrecognized event `%s'", $event);
			}
		}

		/**
		 * Service has changed enablement
		 *
		 * @return bool
		 */
		protected function hasChange(): bool
		{
			if (!$this->ctx->isEdit()) {
				return (bool)$this->ctx->getServiceValue(null, 'enabled');
			}
			$var = strtolower(static::getBaseClassName());

			return $this->force || $this->ctx->getNewServiceValue(null, $var) !==
				$this->ctx->getOldServiceValue(null, $var);
		}

		/**
		 * Get validator description
		 *
		 * @return null|string
		 */
		public function getDescription(): ?string
		{
			return static::DESCRIPTION;
		}

		public function getValidatorRange()
		{
			return static::VALUE_RANGE;
		}

		public function forceValidation(): void
		{
			$this->force = true;
		}

		public function getServiceName(): string
		{
			return strtolower(basename(str_replace('\\', '/', static::class)));
		}

		/**
		 * Map key is in use
		 *
		 * @param string $key
		 * @param string $map
		 * @param null   $checker
		 * @return bool
		 */
		protected function mapKeyInUse(string $key, string $map, $checker = null): bool
		{
			if (false === ($val = Map::load($map, 'cd')->fetch($key))) {
				return false;
			}

			return $checker !== null ? $val !== $checker : true;
		}

		/**
		 * Convert validator into service var
		 *
		 * @param string $class
		 * @return string
		 */
		public static function configize(string $class): string
		{
			static $cache = [];
			if (isset($cache[$class])) {
				return $cache[$class];
			}
			$class = str_replace('\\', '/', $class);
			$cache[$class] = strtolower(basename(\dirname($class)) . '.' . basename($class));

			return $cache[$class];
		}

		/**
		 * Freshen account purging any active sessions
		 *
		 * @param SiteConfiguration $s
		 */
		protected function freshenSite(SiteConfiguration $s): void
		{
			if ($s->getAuthContext()->user_id === \Auth::profile()->user_id && $s->getAuthContext()->group_id === \Auth::profile()->group_id) {
				\Auth::autoload()->setID($s->getAuthContext()->id);
			}
			\apnscpSession::invalidate_by_site_id($s->getSiteId(), $s->getAuthContext()->id);
			$s->getAuthContext()->reset();
		}

	}
