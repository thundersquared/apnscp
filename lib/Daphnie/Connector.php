<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, October 2021
 */

namespace Daphnie;

use Daphnie\Query\Generic;
use Opcenter\Admin\Bootstrapper;
use Opcenter\Admin\Bootstrapper\Config;

class Connector
{
	use \NamespaceUtilitiesTrait;

	private static string $version = '';
	protected \PDO $db;
	protected static $vendors = [];

	/**
	 * Connector constructor.
	 *
	 * @param \PDO $db
	 */
	public function __construct(\PDO $db) {
		$this->db = $db;
	}

	/**
	 * Get TimescaleDB version
	 */
	public function getVersion(): string
	{
		if (self::$version) {
			return self::$version;
		}

		$query = "SELECT extversion FROM pg_extension where extname = 'timescaledb'";
		$rs = $this->db->query($query);
		return self::$version = $rs->fetchObject()->extversion;
	}

	public function upgrade(string $version = null): bool
	{
		self::$version = '';
		$query = "ALTER EXTENSION timescaledb UPDATE";
		if ($version) {
			$query .= " TO " . $this->db->quote($version);
		}
		$rs = $this->db->query($query);

		return $rs->errorCode() ? array_get($rs->errorInfo(), 2) : true;
	}

	/**
	 * Load a version-specific PostgreSQL handler
	 *
	 * @param string $class type of handler to vendor
	 * @return mixed PostgreSQL\Generic\<$class> instance
	 */
	public function vendor(string $class = 'Query'): Generic
	{
		if (isset(self::$vendors[$class])) {
			return new self::$vendors[$class]($this->db);
		}
		$version = $this->getVersion();
		[$maj, $min, $_] = explode('.', $version, 3);
		$namespace = self::getNamespace() . '\\' . $class;
		$class = ucwords($class);
		$try = null;
		for ($i = (int)$min; $i >= -1; $i--) {
			$try = $namespace . '\\v' . $maj . ($i >= 0 ? str_pad((string)$i, 2, '0', STR_PAD_LEFT) : '');
			if (class_exists($try)) {
				$namespace = $try;
				break;
			}
			$try = null;
		}
		if (null === $try) {
			$namespace .= '\\' . $class;
		}
		$instance = new $namespace($this->db);
		self::$vendors[$class] = $namespace;

		return $instance;
	}

	public function raiseSharedLocks(int $n = 32): void
	{
		$cfg = new Config();
		$max = $cfg['pgsql_max_locks_per_transaction'] ?? 128;
		warn("Lock limit reached. Increasing max_locks_per_transaction from %(old)d to %(new)d",
			[
				'old' => $max,
				'new' => $max + $n
			]);
		// advised limit is 2 * number of chunks
		$cfg['pgsql_max_locks_per_transaction'] = $max + 32;
		$cfg->sync();
		Bootstrapper::run('pgsql/install');
	}
}

