<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, November 2019
 */

namespace Daphnie;

	use Daphnie\Contracts\MetricProvider;
	use Daphnie\Metrics\Apache;
	use Daphnie\Metrics\Memory;
	use Daphnie\Metrics\Mysql;
	use Daphnie\Metrics\Php;
	use Daphnie\Metrics\Rampart;

	/**
	 * Metrics interactions for DAPHNIE:
	 *
	 * Distributed Analytics and Predictive Hot, Naive Isostatic Economizer
	 */
	class MetricBroker
	{
		/**
		 * @var array preloaded metrics
		 */
		private static $preloadedGroups = [
			// doubles as metric prefix
			'apache'  => Apache::class,
			'memory'  => Memory::class,
			'mysql'   => Mysql::class,
			'php'     => Php::class,
			'rampart' => Rampart::class
		];

		/**
		 * @var array cached metric maps
		 */
		private static $metrics = [

		];


		/**
		 * Get preloaded metrics
		 *
		 * Invoked by Collector bootstrap
		 *
		 * @return array
		 */
		public static function getPreload(): array
		{
			return static::$preloadedGroups;
		}

		/**
		 * Lookup metric from registered metrics
		 *
		 * @param string $name
		 * @return null|Metric
		 */
		public static function resolve(string $name): ?Metric
		{
			if (isset(static::$metrics[$name])) {
				if (\is_string($class = static::$metrics[$name])) {
					static::$metrics[$name] = new $class;
				}
				return static::$metrics[$name];
			} else if (false !== ($pos = strpos($name, '-'))) {
				$group = substr($name, 0, $pos);
				if (isset(static::$preloadedGroups[$group])) {
					// group hasn't been registered yet
					static::register(static::$preloadedGroups[$group], $group);
					return static::$metrics[$name] ?? null;
				}
			}

			return null;
		}

		/**
		 * Register metric
		 *
		 * Metrics may be registered at boot or as encountered
		 * MetricBroker::register('apache', ApacheMetrics::class)
		 *
		 * @param        $class
		 * @param string $name
		 */
		public static function register($class, string $name = null): void
		{
			if (\is_string($class)) {
				$class = new $class;
			}

			if (!\in_array(MetricProvider::class, class_implements($class), true)) {
				fatal('Provider does not implement %s', MetricProvider::class);
			}
			if (null === $name) {
				$name = $class->metricAsAttribute();
			}
			if (isset(static::$metrics[$name])) {
				debug("Metric `%s' previously registered - ignoring request", $name);
				return;
			}

			if (!$class->isGroup()) {
				static::$metrics[$name] = $class;
			}

			foreach ($class->getGroup() as $metric) {
				$key = "${name}-${metric}";
				if (isset(static::$metrics[$key])) {
					debug("Metric `%s' previously registered - ignoring request", $key);
					continue;
				}
				// @todo weak ref
				static::$metrics[$key] = $class->mutate($metric);
			}
		}
	}
