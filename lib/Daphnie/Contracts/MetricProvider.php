<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, December 2019
 */

namespace Daphnie\Contracts;

interface MetricProvider {
	public const TYPE_VALUE = 'value';
	public const TYPE_MONOTONIC = 'monotonic';

	/**
	 * Get human-readable metric name
	 * @return string
	 */
	public function getLabel(): string;

	/**
	 * Get metric type
	 *
	 * @return string value or monotonic
	 */
	public function getType(): string;
}
