<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, March 2020
 */

namespace Daphnie\Contracts;

use Daphnie\Collector;

/**
 * Interface AnonymousLogging
 *
 * Anonymous logging by telemetry::_cron()
 *
 * @package Daphnie\Contracts
 */
interface AnonymousLogging {
	public function log(Collector $collector): bool;
}

