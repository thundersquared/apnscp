<?php

	namespace Lararia\Providers;

	use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
	use Illuminate\Support\Facades\Route;
	use Lararia\Routing\NamespacedRouteCollection;

	class RouteServiceProvider extends ServiceProvider
	{
		/**
		 * This namespace is applied to your controller routes.
		 *
		 * In addition, it is set as the URL generator's root namespace.
		 *
		 * @var string
		 */
		protected $namespace = '\Laravel\Horizon\Http';

		/**
		 * Define your route model bindings, pattern filters, etc.
		 *
		 * @return void
		 */
		public function boot()
		{
			parent::boot();

			NamespacedRouteCollection::registerDynamicNamespace('@app', static function (string $app) {
				return [
					[
						'as'        => "@app($app)::",
						'namespace' => (new \ReflectionClass(\Page_Container::kernelFromApp($app)))->getNamespaceName(),
						'prefix'    => \Template_Engine::init()->getPathFromApp($app)
					],
					\Page_Container::resolve($app, 'routes.php')
				];
			});
		}

		/**
		 * Define the routes for the application.
		 *
		 * @return void
		 */
		public function map()
		{
			$this->mapApiRoutes();

			$this->mapWebRoutes();

			//
		}

		/**
		 * Define the "api" routes for the application.
		 *
		 * These routes are typically stateless.
		 *
		 * @return void
		 */
		protected function mapApiRoutes()
		{
			Route::prefix('api')
				->middleware('api')
				->namespace($this->namespace)
				->group(config_path('laravel-routes/api.php'));
		}

		/**
		 * Define the "web" routes for the application.
		 *
		 * These routes all receive session state, CSRF protection, etc.
		 *
		 * @return void
		 */
		protected function mapWebRoutes()
		{
			Route::middleware('web')
				->namespace($this->namespace)
				->group(config_path('laravel-routes/web.php'));
		}
	}
