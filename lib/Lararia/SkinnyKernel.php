<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2017
	 */

	declare(strict_types=1);

	namespace Lararia;

	use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

	class SkinnyKernel extends ConsoleKernel implements Contracts\SkinnyKernel
	{
		protected $bootstrappers = [
			//\Illuminate\Foundation\Bootstrap\LoadEnvironmentVariables::class,
			\Illuminate\Foundation\Bootstrap\LoadConfiguration::class,
			\Illuminate\Foundation\Bootstrap\RegisterFacades::class,
			\Illuminate\Foundation\Bootstrap\SetRequestForConsole::class,
			\Illuminate\Foundation\Bootstrap\RegisterProviders::class,
			\Illuminate\Foundation\Bootstrap\BootProviders::class
		];

		/**
		 * Minimum bootstrap
		 *
		 * @param \Symfony\Component\Console\Input\InputInterface $input
		 * @param null                                            $output
		 * @return int
		 */
		public function handle($input, $output = null): int
		{
			$this->bootstrap();

			return 1;
		}
	}