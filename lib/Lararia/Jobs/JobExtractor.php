<?php
	/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2020
 */

	declare(strict_types=1);

	namespace Lararia\Jobs;

	class JobExtractor
	{
		/**
		 * Extract job from event
		 *
		 * @param $event
		 * @return mixed
		 */
		public static function extractJobCommand($event): ?Job
		{
			return \Util_PHP::unserialize(
				array_get(self::extractRawPayload($event), 'data.command', null),
				true
			);
		}

		/**
		 * Extract raw data from job
		 *
		 * @param $event
		 * @return array
		 */
		public static function extractRawPayload($event): array
		{
			if (!empty($event->payload->decoded)) {
				return $event->payload->decoded;
			}

			if (isset($event->payload) && is_string($event->payload)) {
				return json_decode($event->payload, true);
			}

			return empty($event->job) ? [] : $event->job->payload();
		}

		public static function getQueueJob($event): ?Job
		{
			return $event->job ?? null;
		}
	}