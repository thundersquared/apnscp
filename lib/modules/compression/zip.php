<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	/**
	 * @package Compression
	 * Provides zip compression/decompression routines in the file manager
	 */
	class Zip_Filter extends Archive_Base
	{
		public static function extract_files($archive, $dest, array $file = null, ?array $opts = array())
		{
			$zip = new ZipArchive();
			if (!$zip->open($archive)) {
				return error("unable to open archive `%(file)s': %(reason)s", [
					'file'   => $archive,
					'reason' => $zip->getStatusString()
				]);
			}
			$zip->extractTo($dest, $file);
			$zip->close();

			return true;
		}

		public static function list_files($archive, ?array $opts = array())
		{
			$zip = new ZipArchive();
			if (!$zip->open($archive)) {
				return error("unable to open archive `%(file)s': %(reason)s", [
					'file' => $archive,
					'reason' => $zip->getStatusString()
				]);
			}
			$files = [];
			$n = $zip->count();
			for ($i = 0; $i < $n; $i++) {
				$stat = $zip->statIndex($i);
				$name = $stat['name'];
				$files[] = [
					'file_name'   => $name,
					'file_type'   => $name[-1] === '/' ? 'dir' : 'file',
					'can_read'    => true,
					'can_write'   => true,
					'can_execute' => true,
					'size'        => !$stat['size'] ? FILESYSTEM_BLKSZ*1024 : $stat['size'],
					'packed_size' => $stat['comp_size'],
					'crc'         => sprintf("%08X", $stat['crc'] ?? 0),
					'link'        => 0,
					'date'        => $stat['mtime']
				];
			}
			return $files;

		}
	}
