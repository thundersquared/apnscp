<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2022
 */

class Auth_Lookup
{

	/**
	 * URL to perform a lookup in multi-server setup
	 * @{link} https://github.com/apisnetworks/cp-proxy
	 */
	protected const SERVER_LOOKUP_URL = AUTH_SERVER_QUERY;
	protected const SERVER_EXTRA_PARAM = '';
	protected const SERVER_LOOKUP_PARAM = 'domain';
	protected const SERVER_AUTH_HEADER = 'X-Auth-Key';

	/**
	 * Perform API request
	 *
	 * @param string $method
	 * @param string $endpoint
	 * @param array  $params
	 * @return mixed
	 * @throws HTTP_Request2_LogicException
	 */
	protected static function request(string $method, string $endpoint, array $params = [])
	{
		if (!static::SERVER_LOOKUP_URL) {
			return null;
		}

		$method = strtoupper($method);

		if (extension_loaded('curl')) {
			$adapter = new HTTP_Request2_Adapter_Curl();
		} else {
			$adapter = new HTTP_Request2_Adapter_Socket();
		}

		$args = array(
			'adapter'          => $adapter,
			'store_body'       => true,
			'follow_redirects' => true,
		);

		if (is_debug()) {
			$args['ssl_verify_host'] = false;
			$args['ssl_verify_peer'] = false;
		}

		$url = self::SERVER_LOOKUP_URL . '/' . $endpoint;

		if ($params && $method === 'GET') {
			$url .= '?' . http_build_query($params);
			$params = [];
		}

		$http = new HTTP_Request2(
			$url,
			$method,
			$args
		);

		$http->setHeader('Accept-Encoding', 'none');
		$http->setHeader('Content-type', 'application/json');
		$http->setHeader('Accept', 'application/json');
		if (AUTH_SERVER_KEY) {
			$http->setHeader(self::SERVER_AUTH_HEADER, AUTH_SERVER_KEY);
		}

		if (static::SERVER_EXTRA_PARAM) {
			foreach (explode('&', static::SERVER_EXTRA_PARAM) as $param) {
				$tmp = explode('=', $param, 2);
				$params[$tmp[0]] = !empty($tmp[1]) ? $tmp[1] : null;
			}
		}

		if ($params) {
			$http->setBody(json_encode($params));
		}

		try {
			$response = $http->send();
			$code = $response->getStatus();
			switch ($code) {
				case 200:
					break;
				default:
					fatal("URL request failed, code `%d': %s", $code, $response->getReasonPhrase());
					return null;
			}
			$content = $response->getBody();
			if (!$content) {
				error('unable to fetch server lookup response!');
				return null;
			}
		} catch (HTTP_Request2_Exception $e) {
			fatal("fatal error retrieving URL: `%s'", $e->getMessage());
			return null;
		}

		return json_decode($content, true);
	}

	/**
	 * Lookup domain
	 *
	 * @param string $domain
	 * @return array|null
	 */
	public static function lookup(string $domain): ?array
	{
		return self::request('POST', 'lookup', [
			static::SERVER_LOOKUP_PARAM => $domain,
			'full' => (bool)AUTH_SERVER_KEY
		]);
	}

	/**
	 * cp-api version
	 *
	 * @return string
	 * @throws HTTP_Request2_LogicException
	 */
	public static function version(): string
	{
		return array_get(self::request('GET', 'version'), 'data');
	}

	/**
	 * List of servers which domain exists
	 *
	 * @param string $domain
	 * @return array
	 */
	public static function serverLocator(string $domain, bool $full = true): array
	{
		return (array)self::request('GET', "domains/server/$domain", ['full' => $full]);
	}

	public static function parent(string $domain): ?string
	{
		return array_get(self::request('GET', "domains/parent/$domain"), 'data');
	}

	public static function all(): ?array
	{
		return self::request('GET', 'domains/all');
	}

	/**
	 * Extended metadata available through cp-api
	 *
	 * @return bool
	 */
	public static function extendedAvailable(): bool
	{
		return (bool)AUTH_SERVER_KEY;
	}
}