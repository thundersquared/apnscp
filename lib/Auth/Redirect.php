<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	/**
	 * Perform a URL redirect if domain not present on server
	 */
	class Auth_Redirect
	{
		const SEEN_SERVER_COOKIE = 'cp-server-seen';

		/**
		 * general layout of all control panel instances:
		 * http://CP_LAYOUT:CP_PORT/
		 */
		const CP_LAYOUT = AUTH_SERVER_FORMAT;
		const CP_PORT = 2082;
		const CP_SSL_PORT = 2083;
		const REDIRECT_CODE = 307;

		public static function redirect($domain): ?bool
		{
			$servers = array();
			if (isset($_COOKIE[static::SEEN_SERVER_COOKIE])) {
				$servers = Util_PHP::unserialize(base64_decode($_COOKIE[static::SEEN_SERVER_COOKIE]));
			}
			if (static::_seenSelf($servers)) {
				return false;
			}

			$server = static::lookup($domain);
			$servers[] = $server;
			// this can be an ephemeral cookie to try at most
			// 2 servers, since lookup() will only return 1 server
			// and the proxy endpoint can be on at most one server
			//
			// **encode cookie** to ensure no spec-breaking line breaks
			setcookie(static::SEEN_SERVER_COOKIE, base64_encode(serialize($servers)), time() + 5);

			// domain deleted from server, server taken from
			// apnscp.subscription_notices table, domain no longer exists
			if (!$server || $server === SERVER_NAME_SHORT) {
				return false;
			}

			$hostname = static::makeCPFromServer($server);
			if (AUTH_SERVER_VALIDITY && !Net_Gethost::gethostbyname_t($hostname, 2500)) {
				return error("domain resides on unknown or retired server `%s', " .
					'check last account payment', $server);
			}
			// cancel anvil tick if site is elsewhere
			\Auth_Anvil::decrement();
			static::send($hostname);

			return true;
		}

		private static function _seenSelf($servers)
		{
			return in_array(SERVER_NAME_SHORT, $servers, true);
		}

		/**
		 * Lookup server name
		 *
		 * @param string $domain
		 * @return bool|null|string false on failure, null on no result, string on match
		 * @throws HTTP_Request2_LogicException
		 */
		public static function lookup(string $domain, bool $full = false)
		{
			$data = \Auth_Lookup::lookup($domain);
			if (empty($data['status'])) {
				return null;
			}
			return is_array($data['data']) ? array_get($data, 'data.server') : $data['data'];
		}

		/**
		 * Create redirection-friendly hostname
		 *
		 * @param $server
		 * @return string
		 */
		public static function makeCPFromServer(string $server): string
		{
			// server provided as cp1.apiscp.com, CP_LAYOUT is <SERVER>.apiscp.com - filter common
			$filter = str_replace('<SERVER>', '', self::CP_LAYOUT);
			if ($filter && str_ends_with($server, $filter)) {
				$server = substr($server, 0, -\strlen($filter));
			}

			return str_replace('<SERVER>', $server, static::CP_LAYOUT ?: '<SERVER>');
		}

		/**
		 * Process the response, then perform
		 * an immediate redirect
		 *
		 * @param $hostname
		 */
		public static function send(string $hostname): void
		{
			if (Util_HTTP::isSecure()) {
				// Util_HTTP::isSecure() won't work
				// as it takes whatever is forwarded from upstream proxy
				$extraport = static::CP_SSL_PORT;
				$url = 'https://' . $hostname;
			} else {
				$extraport = static::CP_PORT;
				$url = 'http://' . $hostname;
			}
			if (static::CP_PORT !== 80 && static::CP_PORT !== 443) {
				$url .= ':' . $extraport;
			}
			$url .= $_SERVER['REQUEST_URI'];
			header('Location: ' . $url, true, static::REDIRECT_CODE);
			exit();
		}

		/**
		 * Get preferred panel URI
		 *
		 * @return string
		 */
		public static function getPreferredUri(): string
		{
			if (MISC_CP_PROXY) {
				return MISC_CP_PROXY;
			}

			return 'https://' . self::makeHostFromServer(SERVER_NAME, self::CP_SSL_PORT);
		}

		/**
		 * Create hostname:port from server
		 *
		 * @param string   $server
		 * @param int|null $port
		 * @return string
		 */
		public static function makeHostFromServer(string $server, ?int $port = self::CP_SSL_PORT)
		{
			$uri = static::makeCPFromServer($server);

			return $uri . ($port ? ':' . $port : '');
		}

	}
