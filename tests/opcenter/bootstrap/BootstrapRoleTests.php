<?php
	require_once dirname(__DIR__, 2) . '/TestFramework.php';

	class BootstrapRoleTests extends TestFramework
	{
		public function testRoleDiscovery()
		{
			$this->assertContains('custom/installed', \Opcenter\Admin\Bootstrapper::roles());
		}

		public function testRoleExpansion()
		{
			$roles = \Opcenter\Admin\Bootstrapper::roles();
			$requested = [];
			foreach ($roles as $role) {
				if (0 === strpos($role, 'mail/')) {
					$requested[] = $role;
				}
			}
			$this->assertSame($requested, \Opcenter\Admin\Bootstrapper::match('mail/*'), 'Role expansion matches');
		}
	}