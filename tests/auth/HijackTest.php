<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class HijackTest extends TestFramework
	{
		public function testHijack()
		{
			\Auth::use_handler_by_name('UI');
			$this->assertSame('UI', \Auth::get_driver()->getDriver());

			$adminCtx = \Auth::profile();
			$adminAfi = \apnscpFunctionInterceptor::factory($adminCtx);
			$this->assertTrue(\Auth::autoload()->session_valid(), 'Session valid');
			$this->assertNotEmpty($_SESSION);
			$acct = \Opcenter\Account\Ephemeral::create();
			$ctx = $acct->getContext();

			$this->assertNotEmpty($_SESSION['valid']);
			$this->assertNotEmpty($hijackSession = $adminAfi->admin_hijack($ctx->getAccount()->site),
				'Hijack ID generated');
			$adminAfi = \apnscpFunctionInterceptor::init();

			$this->assertNotEmpty($_SESSION['valid']);

			$overrodeContext = \Auth::profile();
			$this->assertSame($ctx->username, $adminAfi->common_whoami(), 'Hijack contexted into user');
			$this->assertSame($overrodeContext->username, $ctx->username);


			$this->assertTrue(\Auth::autoload()->session_valid(), 'Hijack session valid');
			$this->assertTrue(\Auth::authenticated());

			$this->assertNotEmpty($_SESSION['valid']);
			$auth = \Auth::autoload()->resetID($adminCtx->id);
			$this->assertNotEmpty($_SESSION['valid']);
			$this->setApnscpFunctionInterceptor(\apnscpFunctionInterceptor::factory($auth->getProfile()));
			$auth->authInfo(true);
			$this->assertNotEmpty($_SESSION);
			$this->assertNotEmpty($_SESSION['valid']);

			\Auth::autoload()->endImpersonation($overrodeContext);

			$adminAfi = \apnscpFunctionInterceptor::factory($adminCtx);
			$this->assertNotEmpty($_SESSION['valid']);
			\Auth::autoload()->setID($adminCtx->id);
			$this->assertNotEmpty($_SESSION);
			$this->assertNotEmpty($_SESSION['valid']);
			$this->assertSame($adminCtx->username, $adminAfi->common_whoami(), 'Hijack contexted into user');
			$this->assertTrue(\Auth::autoload()->session_valid(), 'Hijack session valid');
		}
	}
