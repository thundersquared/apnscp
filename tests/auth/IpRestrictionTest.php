<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class IpRestrictionTest extends TestFramework
	{
		public function testUsage()
		{
			$ephemeral = \Opcenter\Account\Ephemeral::create();
			$ip = long2ip(random_int(1, 2**32-1));
			$afi = $ephemeral->getApnscpFunctionInterceptor();
			$this->assertArrayNotHasKey($ip, $afi->auth_get_ip_restrictions());
			$this->assertTrue($afi->auth_restrict_ip($ip));
			$this->assertArrayHasKey($ip, $afi->auth_get_ip_restrictions());
			$this->assertTrue($afi->auth_remove_ip_restriction($ip));
			$this->assertArrayNotHasKey($ip, $afi->auth_get_ip_restrictions());
		}

		public function testAuthorization()
		{
			$ephemeral = \Opcenter\Account\Ephemeral::create([
				'auth.tpasswd' => ($pass = \Opcenter\Auth\Password::generate())
			]);

			$randomIp = long2ip(random_int(1, 2 ** 32 - 1));
			$afi = $ephemeral->getApnscpFunctionInterceptor();
			$restrictor = \Auth\IpRestrictor::instantiateContexted($ephemeral->getContext());
			$restrictor->add($randomIp);

			$fakeIp = long2ip(random_int(1, 2 ** 32 - 1));;
			$this->assertNotEquals($randomIp, $fakeIp);

			$originalIp = \Auth::client_ip();
			$_ENV[\Auth::CLIENT_ENV_IP] = $fakeIp;

			$this->assertNotContains(\Auth::client_ip(), $restrictor->list());
			$auth = \Auth::import('UI');
			$auth->setID($ephemeral->getContext()->id);

			$this->assertFalse(
				$auth->verify($afi->common_whoami(), $pass, $afi->common_get_service_value('siteinfo', 'domain'))
			);

			$_ENV[\Auth::CLIENT_ENV_IP] = $originalIp;
			$this->assertTrue(
				$auth->verify($afi->common_whoami(), $pass, $afi->common_get_service_value('siteinfo', 'domain'))
			);

			if (!NO_AUTH || \Auth::authenticated()) {
				$this->markTestIncomplete("NO_AUTH or session authentication present. See public build d8bbae8e");
			}

		}
	}