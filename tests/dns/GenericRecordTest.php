<?php
	require_once dirname(__DIR__) . '/TestFramework.php';
	require_once(__DIR__ . '/helpers/RecordFramework.php');

	class GenericRecordTest extends TestFramework
	{
		const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;

		public function testRecordMatch()
		{

			$r1 = new \Opcenter\Dns\Record('_dummy_zone.com', [
				'name'      => 'foobar',
				'rr'        => 'A',
				'parameter' => '1.2.3.4'
			]);
			$r2 = Dns_Module::createRecord('_dummy_zone.com', [
				'name'      => 'barbaz',
				'rr'        => 'A',
				'parameter' => '1.2.3.4'
			]);

			$this->assertFalse($r1->is($r2));
			$r2['name'] = 'foobar';
			$this->assertTrue($r1->is($r2));
		}

		public function testRecordMeta()
		{
			$r1 = new \Opcenter\Dns\Record('_dummy_zone.com', [
				'name'      => 'foobar',
				'rr'        => 'MX',
				'parameter' => '10 bar.baz.com.'
			]);
			$r2 = Dns_Module::createRecord('_dummy_zone.com', [
				'name'      => 'foobar',
				'rr'        => 'MX',
				'parameter' => '10 bar.baz.com'
			]);

			$this->assertTrue($r1->is($r2));
			$r2->setMeta('priority', "20");
			$this->assertFalse($r1->is($r2));
			$this->assertSame("20 bar.baz.com", $r2['parameter']);
		}
	}

