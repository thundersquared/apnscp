<div class="card">
	<div class="card-header" role="tab" id="headingFilesystem">
		<h5 class="mb-0">
			<a class="btn-block" data-toggle="collapse" data-parent="#accordion" href="#filesystem"
			   aria-expanded="true"
			   aria-controls="filesystem">
				Filesystem
			</a>
		</h5>
	</div>
	<div id="filesystem" class="collapse" role="tabpanel" aria-labelledby="headingFilesystem">
		<?php $fsinfo = $Page->filesystem_information(); ?>
		<div class="card-block">
			<table width="100%" class="table">
				<thead>
				<tr>
					<th class="">
						Mount
					</th>
					<th class="">
						Type
					</th>
					<th class="">
						Type
					</th>
					<th class="">
						Percent Capacity
					</th>
					<th class="right">
						Free
					</th>
					<th class="right">
						Used
					</th>
					<th class="right">
						Size
					</th>
				</tr>
				</thead>

				<?php
				foreach($fsinfo as $device):
				?>
				<tr>
					<td>
						<?=$device['disk']?>
					</td>
					<td>
						<?=$device['fstype']?>
					</td>
					<td>
						<?=$device['fstype']?>
					</td>
					<td>
						<?php printf("<div class='ui-gauge' style='display:block;'>
						<div class='ui-gauge-slice ui-gauge-used gauge-used' style='width:%d%%'></div>
						<div class='ui-gauge-slice ui-gauge-free gauge-free' style='width:%d%%'></div>
		</div>
		<span class='ui-gauge-label'>%.2f%%</span>",
							$device['percent'],
							(100 - (int)$device['percent']),
							($device['used']) / $device['size'] * 100) ?>
					</td>
					<td class="right">
						<?=\Formatter::commafy(\Formatter::reduceBytes($device['free'] * 1024))?>
					</td>
					<td class="right">
						<?=\Formatter::commafy(\Formatter::reduceBytes($device['used'] * 1024))?>
					</td>
					<td class="right">
						<?=\Formatter::commafy(\Formatter::reduceBytes($device['size'] * 1024)) ?>
					</td>
				</tr>
				<?php
				endforeach;
				?>
			</table>
		</div>
	</div>
</div>