@component('theme::partials.app.modal')
    @slot('title')
        Fortification &ndash; {{ $app->getHostname() }}
    @endslot
    @slot('id')
        writeModal
    @endslot
    <div>
        <h5></h5>
        <div class="">
            <hr/>
            <fieldset class="form-group">
                <h6>Duration</h6>
                <label class="custom-control custom-checkbox mb-0 pl-0 align-items-center">
                    <input name="wa-duration" type="number" min="1" max="2880" class="form-control form-control-lg" value="10"
                           required/>
                    <span class="ml-1">
						minutes
					</span>
                </label>
                <div class="help-block with-errors text-danger"></div>
            </fieldset>
        </div>
    </div>
    @slot('buttons')
        <input type="hidden" name="hostname" value="{{ $app->getHostname() }}" />
        <input type="hidden" name="path" value="{{ $app->getPath() }}"/>
        <button type="submit" name="webappwrite" class="btn btn-primary ajax-wait">
            <i class="fa fa-pulse d-none"></i>
            Apply mode
        </button>
        <p class="ajax-response"></p>
    @endslot
@endcomponent