<a href="/apps/filemanager?cwd={{ $app->getAppRoot() }}" class="mb-3 btn btn-secondary" id="fileManager">
	<i class="ui-action ui-action-label ui-menu-category ui-menu-category-files ui-action-d-compact"></i>
	File Manager
</a>

@include('partials.modals.duplicate')