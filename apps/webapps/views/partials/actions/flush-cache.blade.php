<button type="submit" class="mb-3 btn btn-secondary" id="flushCache"
   data-toggle="tooltip" data-title="Empty PageSpeed cache"
   href="{{  HTML_Kit::new_page_url_params(null, ['purge' => $pane->getHostname()]) }}">
	<i class="ui-action ui-action-label ui-action-refresh ui-action-d-compact"></i>
	Flush Cache
</button>