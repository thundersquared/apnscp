<script>
	$('#portSelection :checkbox').change(function () {
		$(this).parentsUntil('.input-group').siblings('.port-input').prop('disabled', !$(this).prop('checked'));
	}).change();
</script>

<div id="portSelection">
	@foreach (['Non-SSL Port', 'SSL Port'] as $k => $v)
		<div class="mb-3 mr-3 input-group">
			<div class="input-group-addon col-3 col-md-2 text-center font-weight-bold">{{ $v }}</div>
			<div class="input-group-addon col-4 col-md-3">
				<label class="custom-switch custom-control mb-0 pl-0 mr-0 align-items-center">
					<input type="hidden" name="args[{{ $k }}]" value="false"/>
					<input type="checkbox" @if ($value[$k]) checked @endif @if ($k === 0) DISABLED @endif
						class="custom-control-input self-submit" value="{{ $value[$k] }}"/>
					<span class="custom-control-indicator align-self-start"></span>
					Enable
				</label>
			</div>

			<input type="number" name="args[{{ $k }}]" min="1" max="65535" value="{{ $value[$k] }}"
		       @if (!$value[$k]) DISABLED @endif class="port-input form-control" />
		</div>
	@endforeach
</div>

