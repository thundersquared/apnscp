<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\logrotate;

	class Page extends \Page_Container
	{
		private $edit_mode;
		private $default_options =
			array(
				'frequency'     => 'daily',
				'compress'      => 1,
				'hold_logs'     => 5,
				'size'          => 0,
				'mail'          => '',
				'mailfirst'     => true,
				'delay'         => 1,
				'rotation_type' => 'periodic'
			);

		public function __construct()
		{
			$this->edit_mode = 'basic';
			if (isset($_GET['mode']) && $_GET['mode'] == 'advanced') {
				$this->edit_mode = 'advanced';
			}
			parent::__construct();
			if (!$this->is_postback) {
				$this->default_options = $this->get_default_options();
			}
			$this->add_script('/apps/logrotate/logrotate.js');
		}

		public function get_default_options()
		{
			$options =
				array(
					'frequency'     => 'daily',
					'compress'      => 0,
					'hold_logs'     => 4,
					'size'          => 0,
					'mail'          => '',
					'delay'         => 0,
					'rotation_type' => 'periodic',
					'dateext'       => 0
				);
			$contents = $this->file_get_file_contents('/etc/logrotate.conf');

			foreach (explode("\n", $contents) as $line) {
				$this->parse_options($line, $options);
			}

			return array_merge($this->default_options, $options);
		}

		private function parse_options($line, &$options = array())
		{
			static $scope;
			if (is_null($scope)) {
				$scope = 0;
			} // 0 = global

			if (preg_match('/^\s*#/', $line)) {
				return false;
			}
			$tokens = preg_split('/\s+(?=\w)/', trim($line));
			if (substr(trim($line), -1, 1) == '{') {
				$scope++;
			}

			if (!$scope) {
				switch (strtolower($tokens[0])) {
					case 'daily':
					case 'monthly':
					case 'yearly':
					case 'weekly':
						$options['rotation_type'] = 'periodic';
						$options['frequency'] = $tokens[0];

						return true;
					case 'size':
						if (!isset($tokens[1])) {
							return true;
						}
						$options['rotation_type'] = 'size';
						$options['size'] = intval($tokens[1]);
						$len = strlen($options['size']);
						$suffix = substr($tokens[1], $len);
						switch ($suffix) {
							case 'M':
							case 'm':
								break;
							case '':
							case 'b':
							case 'B':
								$options['size'] /= 1024;
							case 'K':
							case 'k':
								$options['size'] /= 1024;
								break;
							default:
								warn("Unknown size `$suffix'");
						}

						return true;
					case 'compress':
						$options['compress'] = 1;

						return true;
					case 'mail':
						$options['mail'] = $tokens[1];

						return true;
					case 'delaycompress':
						if (!isset($tokens[1])) {
							return true;
						}
						$options['delay'] = 1;

						return true;
					case 'rotate':
						if (!isset($tokens[1])) {
							return true;
						}
						$options['hold_logs'] = $tokens[1];

						return true;
				}
			} else {
				if ($tokens[0] == '}') {
					$scope--;
				}

				return false;
			}

		}

		// Used by Basic mode

		public function on_postback($params)
		{
			$this->default_options = $this->get_default_options();
			if (!isset($params['Save'])) {
				$this->hide_pb();

				return false;
			}
			if ($this->edit_mode == 'basic') {
				$newconf = array();
				if ($params['rotation_type'] == 'periodic') {
					$this->default_options['frequency'] = $params['periodic'];
					$newconf[$params['periodic']] = null;
					unset($this->default_options['size']);
				} else {
					$params['size'] .= 'M';
					$this->default_options['size'] = $params['size'];
					$newconf['size'] = $params['size'];
					unset($this->default_options['frequency']);
				}
				$this->default_options['mail'] = $params['email_address'];
				$this->default_options['compress'] = isset($params['compress']);
				$this->default_options['hold_logs'] = $params['hold_logs'];
				$this->default_options['delay'] = isset($params['defer']);
				$this->default_options['dateext'] = !empty($params['dateext']);

				$logrotate_conf = $this->file_get_file_contents('/etc/logrotate.conf');
				if ($this->default_options['mail']) {
					$newconf['mail'] = $params['email_address'];
				}
				if ($this->default_options['compress']) {
					$newconf['compress'] = null;
				}
				if ($this->default_options['compress'] && $this->default_options['delay']) {
					$newconf['delaycompress'] = null;
				}
				if ($this->default_options['hold_logs'] >= 0) {
					$newconf['rotate'] = $this->default_options['hold_logs'];
				}
				if ($this->default_options['dateext']) {
					$newconf['dateext'] = null;
				}
				foreach (explode("\n", $logrotate_conf) as $line) {
					if ($this->parse_options($line)) {
						continue;
					}
					$newconf[$line] = null;
				}

				// post processing
				if (!$params['email_address']) {
					unset($newconf['email']);
				}
				$logrotate_txt = '';
				foreach ($newconf as $key => $val) {
					$logrotate_txt .= $key . ($val ? ' ' . $val : '') . "\n";
				}
				$this->logs_set_logrotate($logrotate_txt);

			} else {
				// advanced hand-edit mode
				if (isset($params['create'])) {
				} else if (isset($params['save'])) {
				}

			}
			//print '<code><pre>'.var_export($params,true).'</pre></code>';


		}

		public function get_edit_mode()
		{
			if ($this->edit_mode == 'advanced') {
				return 'advanced';
			} else {
				return 'basic';
			}
		}

		////////////////////////////////////////////

		// Used by Advanced mode

		public function get_default_logrotate()
		{
			return $this->file_get_file_contents('/etc/logrotate.conf');
		}
	}

?>
