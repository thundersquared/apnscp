@php
	$users = $Page->getUsers();
	$mydb = $_GET['db'] ?? null;

	$dbUsers = array();
	foreach ($users as $user => $optData) {
		krsort($optData);
		foreach (array_keys($optData) as $host) {
			$privs = $mydb ? $Page->getPerms($user, $host, $mydb) : [];
			$dbUsers[] = array(
				'user'  => $user,
				'host'  => $host,
				'rank'  => array_sum($privs),
				'privs' => $privs
			);
		}
	}

	$userActive = $_GET['edit'] ?? false;
	$dbActive = $_GET['db'] ?? false;

	uasort($dbUsers, array(apps\changemysql\Page::class, 'sort_perm_rank'));
@endphp
<form method="post" class="row mb-3" action="{{ \HTML_Kit::page_url_params() }}">
	<div class="col-12">
		@if (!$Page->show_hosts())
			<p class="note">
				Clients that connect remotely are currently hidden.
				<a href="{{ \HTML_Kit::page_url_params(array('hosts' => 'show'), true) }}">
					Display remote clients
				</a>
			</p>
		@else
			<p class="note">
				Clients that connect remotely are currently visible.
				<a href="{{ \HTML_Kit::page_url_params(array('hosts' => 'hide'), true) }}">
					Hide remote clients
				</a>
			</p>
		@endif

		<h3>
			<button type="button" class="btn btn-secondary collapse-toggle" name="expandDB" data-toggle="collapse"
			        data-target="#dbContainer" aria-expanded="{{ $dbActive ? 'true' : 'false' }}"
			        aria-controls="dbContainer">
				<i class="fa fa-caret-down fa-rotate-vertical"></i>
			</button>
			Edit Databases
		</h3>
		<div id="dbContainer" class="row collapse @if ($dbActive) show @endif">
			@includeWhen(isset($_GET['db']) && cmd('sql_mysql_database_exists', $_GET['db']), 'partials.edit-db')

			<div class="col-12 col-sm-6 col-md-5 col-lg-4 float-sm-left">
				<ul class="list-unstyled list-group left-pane" id="database-list">
					@php
						$html = array();
					@endphp
					@foreach($Page->getDatabases() as $db)
						<li class="@if ($db === $mydb) active @endif list-group-item">
							@includeWhen($db === $mydb, 'partials.db.edit-active', ['mydb' => $db])
							@if ($db !== $mydb)
								<a class="btn-block" href="{{ \HTML_Kit::page_url_params(array(
										'db'   => $db,
										'edit' => null,
										'host' => null
									)) }}" rel="{{ $db }}">
							@endif
							<div class="w-100">
								<i class="fa fa-database mr-2"></i>{{ $db }}
							</div>
							@if ($db !== $mydb)
								</a>
							@else
							<div class="w-100">
								<i class="fa fa-hdd-o"></i>
								{{ \Formatter::reduceBytes(\cmd('mysql_get_database_size', $mydb)) }}
							</div>
							@endif
						</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
</form>
@php
	$myuser = $myhost = $myperms = null;
	$dbPrefix = $Page->getDBPrefix();
	if (isset($_GET['edit']) && isset($_GET['host'])) {
		if (!$_GET['host']) $myhost = 'localhost';
		else $myhost = $_GET['host'];
		$myuser = $_GET['edit'];
		if (isset($users[$myuser][$myhost])) {
			$myperms = $users[$myuser][$myhost];
		}
	}
@endphp

<form method="post" class="row mb-3" action="{{ \HTML_Kit::page_url_params() }}" data-toggle="validator">
	<div class="col-12">

		<h3>
			<button type="button" class="btn btn-secondary collapse-toggle" name="expandDB" data-toggle="collapse"
			        data-target="#userContainer"
			        aria-expanded="@if ($userActive) true @else false @endif" aria-controls="userContainer">
				<i class="fa fa-caret-down fa-rotate-vertical"></i>
			</button>
			Edit Users
		</h3>

		<div class="row collapse @if ($userActive) show @endif" id="userContainer">
			@if ($myuser && isset($users[$myuser]))
			<div class="col-12 col-sm-6 col-md-8 col-lg-8 float-sm-right">
				<div class="clearfix row">
					@php
						$userLen = \cmd('mysql_schema_column_maxlen', 'user') - strlen($Page->getDBPrefix());
						$mainUser = $_SESSION['username'];
						$prettyUser = !strncmp($myuser, $dbPrefix, strlen($dbPrefix)) ?
							substr($myuser, strlen($dbPrefix)) : $myuser;
					@endphp
					<input type="hidden" name="users[state]" value="{{  $myuser . " " . $myhost }}"/>
					<fieldset class="form-group col-12 col-lg-6">
						<label>User</label>
						<div class="input-group">
							<span class="input-group-addon flex-row">
                                <i class="fa fa-user mr-1 align-self-center"></i>
                                @if ($myuser != $_SESSION['username']) {{ $dbPrefix }} @endif
                            </span>
							<input type="text" name="" class="form-control" disabled="DISABLED"
							       value="{{ $prettyUser }}"/>
						</div>
					</fieldset>

					<fieldset class="form-group has-feedback col-12 col-lg-6">
						<div class="row">
							<label class="col-md-12">Password</label>
							<div class="col-md-12 input-group">
								<div class="input-group-addon"><i class="fa fa-lock"></i></div>
								<input id="password" type="password" data-minlength="6" class="form-control"
								       name="password" value="" id="password"/>
							</div>
							<div class="help-block col-12">
								{{ AUTH_MIN_PW_LENGTH }} character minimum
							</div>
						</div>
					</fieldset>

					<fieldset class="form-group col-12 col-lg-6 password-confirm-container">
						<div class="row">
							<label class="col-md-12">Verify Password</label>
							<div class="col-md-12 input-group">
								<div class="input-group-addon"><i class="fa fa-lock"></i></div>
								<input type="password" data-match-error="Password does not match"
								       class="form-control form-control-error"
								       data-match="#password" name="password_confirm" value="" id="password_confirm"/>
							</div>
							<div class="col-12 help-block with-errors text-danger"></div>
						</div>
					</fieldset>

					<div class="form-group btn-group col-lg-6 col-12 d-flex align-items-end">
						<button type="submit" id="save-user" name="Save_User" class="btn btn-primary" value="Save">
							Save
						</button>
						<a class="btn btn-secondary dropdown-toggle ui-action ui-action-advanced ui-action-label"
						   data-toggle="collapse" aria-controls="userAdvanced" data-target="#userAdvanced"
						   aria-expanded="false">
							<span class="sr-only">Toggle Advanced</span>
						</a>
					</div>
				</div>

				<div id="userAdvanced" class="mt-1 collapse">
					<div class="row">
						<fieldset class="form-group  col-12 col-md-4">
							<label>Host</label>
							<input type="text" class="form-control" name="users[edit][host]" value="{{ $myhost }}"
							       maxlength="60" @if (!$Page->show_hosts() || $myhost === 'localhost' && $myuser === \cmd('common_get_service_value', 'mysql', 'dbaseadmin')) readonly @endif />
						</fieldset>

						<fieldset class="form-group  col-12 col-md-4">
							<label>Max Connections</label>
							<input min=0 max={{ \Mysql_Module::MAX_CONCURRENCY_LIMIT }} name="users[edit][max_user_connections]"
							       value="{{ $myperms['max_user_connections'] }}" class="form-control"/>
						</fieldset>

						<fieldset class="form-group col-12 col-md-4">
							<label>Max Updates</label>
							<input type="number" maxlength="5" size="2" step="50" name="users[edit][max_updates]"
							       value="{{ $myperms['max_updates'] }}" class="form-control"/>
						</fieldset>

						<fieldset class="form-group col-12 col-md-4">
							<label>Max Questions</label>
							<input type="number" maxlength="5" size="2" step="50" name="users[edit][max_questions]" class="form-control"
							       value="{{ $myperms['max_questions'] }}"/>
						</fieldset>

						<div class="form-group col-12 col-md-4 d-flex align-items-end">
							<label class="custom-control mb-2 custom-checkbox mb-0 align-items-center">
								<input type="checkbox" class="form-check-input custom-control-input"
								       name="users[edit][use_ssl]"
								@if ($myperms['ssl_type']) checked="CHECKED" @endif />
								Require SSL
								<span class="custom-control-indicator"></span>
							</label>
						</div>

						<fieldset class="form-group col-12 col-md-4">
							<label>SSL Type</label>
							<select class="form-control custom-select" name="users[edit][ssl_type]">
								<option
									@if ($myperms['ssl_type'] == 'ANY') SELECTED @endif value="">
									ANY
								</option>
								<option
									@if ($myperms['ssl_type'] == 'X509') SELECTED @endif value="X509">
									Valid X509
								</option>
							</select>
						</fieldset>

						<h4 class="col-12">SSL Requirements</h4>
						<fieldset class="form-group col-12 col-md-4">
							<label>Cipher Type</label>
							<textarea rows="5" class="form-control"
							          name="users[edit][ssl_cipher]">{{ $myperms['ssl_cipher'] }}</textarea>
						</fieldset>

						<fieldset class="form-group col-12 col-md-4">
							<label>X509 Issuer</label>
							<textarea rows="5" class="form-control"
							          name="users[edit][x509_issuer]">{{ $myperms['x509_issuer'] }}</textarea>
						</fieldset>

						<fieldset class="form-group col-12 col-md-4">
							<label>X509 Subject</label>
							<textarea rows="5" class="form-control"
							          name="users[edit][x509_subject]">{{ $myperms['x509_subject'] }}</textarea>
						</fieldset>
					</div>
				</div>
			</div>

			@endif

			<div class="col-12 col-sm-6 col-md-4 col-lg-4 float-sm-left">
				<ul class="list-unstyled list-group left-pane" id="user-list">
					@php
						$html = array();
					@endphp
					@foreach($users as $user => $listData)
						@php
							krsort($listData);
						@endphp
						@continue(!$Page->show_hosts() && !isset($listData['localhost']))
						<li class="@if ($myuser && $user == $myuser) active @endif list-group-item list-group-heading">
							<div class='d-block w-100'>
							@if ($myuser && $user === $myuser)
								<div class="btn-group align-self-end ml-auto">
									<button type="submit" value="Save" class="grants-save btn btn-primary"
									        name="Save_User">
										Save
									</button>
									@if ($myuser != $_SESSION['username'])
										<button type="submit" class="btn btn-secondary warn "
										        name="users[delete][{{ $myuser }}][{{ $myhost }}]"
										        onClick="return confirm('Are you sure you want to delete the user {{ $myuser }}{{ '@' . $myhost }}?');">
											<i class="ui-action ui-action-delete"></i>
										</button>
									@endif
								</div>
							@endif
							@if ($Page->show_hosts())
							<i class="fa fa-list-item fa-user mr-2 align-self-start"></i>{{  $user }}
							@endif
						</div>
						@if ($Page->show_hosts())
							<ul class="list-unstyled">
						@endif
						@foreach ($listData as $host => $perms)
							@continue(!$Page->show_hosts() && $host != 'localhost')
							@if (!($isactive = ($myuser && $user == $myuser && ($host == $myhost || !$Page->show_hosts()))))
								<a href="{{ \HTML_Kit::page_url_params(array(
										'edit' => $user,
										'host' => $host,
										'db'   => null
									)) }}" class="d-block" rel="{{ $user . " " . $host }}">
							@endif

							@if ($Page->show_hosts())
								<span class="block">
									<i class="fa fa-list-item fa-laptop mr-2"></i><span class="@if ($isactive) font-weight-bold @endif">{{ $host }}</span>
								</span>
							@else
								<span class="block">
									<i class="fa fa-list-item fa-user mr-2 align-self-start"></i>{{  $user }}
								</span>
							@endif
							@if (!$isactive)
								</a>
							@endif
							@if ($Page->show_hosts())
								</li>
							@endif
						@endforeach
						@if ($Page->show_hosts())
						</ul>
						@endif
					@endforeach
				</ul>
			</div>
		</div>
	</div>
</form>

@modal(['id' => "snapshotModal", 'title' => "Snapshot success!"])
	<div>
		<div class="mb-1 alert alert-success">
			<i class="fa fa-database"></i>
			<span class="dbname"></span>
			has been saved to
			<span class="filename"></span>
		</div>
		<p class="note small">
			Remember to manually delete these files after use
			(Files &gt; <a class="ui-action ui-action-label ui-action-switch-app"
			               href="<?php print \HTML_Kit::new_page_url_params('/apps/filemanager',
							   array('f' => \Util_Conf::home_directory() . '/mysql_backups'));?>">File
				Manager</a>)
			otherwise the snapshot will be automatically deleted after 5 days.
		</p>
	</div>
@endmodal

@modal(['id' => 'importModal', 'title' => '', 'action' => \HTML_Kit::page_url()])
	@slot('headerBody')
		<h4 class="modal-title db-name"></h4>
	@endslot

	<h5>Restore from backup</h5>
	<div class="">
		<div class="form-group">
			<ul class="backup-restore list-unstyled mb-0" id="db-list">
			</ul>
			<p class="note">
				No backups configured for <span class="db-name"></span>. Configure in
				<a href="/apps/mysqlbackups" class="ui-action ui-action-label ui-action-switch-app">MySQL
					Backups</a>.
			</p>
			<div class="help-block with-errors text-danger"></div>
		</div>
		<hr/>
		<fieldset class="form-group">
			<label class="custom-control custom-checkbox mb-0">
				<input name="enable" type="checkbox" class="custom-control-input" value="1" id="confirm"
				       required/>
				<span class="custom-control-indicator"></span>
				<span>
					I understand that importing a database backup will delete all
					data presently in <b class="db-name"></b>. This cannot be undone.
				</span>
			</label>
			<div class="help-block with-errors text-danger"></div>
		</fieldset>
	</div>
	@slot('buttons')
		<button type="submit" name="export[]" value="1" data-validate="false"
		        class="btn btn-secondary export">
			<i class="fa fa-download"></i>
			Export Database
		</button>
		<button type="submit" class="btn btn-primary import ajax-wait">
			<i class="fa fa-pulse"></i>
			Import
		</button>
		<p class="ajax-response"></p>
	@endslot
@endmodal
