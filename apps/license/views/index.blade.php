<div class="row">
	<div class="col-12">
		@if (!$license->hasExpired())
			<h2 class="text-success">License is VALID</h2>
			<p class="alert-success p-3">
				Your license is valid and expires
				on {{ (new DateTime())->setTimestamp(array_get($license->getLicense(), 'validTo_time_t', time()))->format('r') }}
			</p>
		@else
			<h2 class="text-danger">License is INVALID</h2>
			<p class="alert-danger p-3">
				Your license expired
				on {{ (new DateTime())->setTimestamp(array_get($license->getLicense(), 'validTo_time_t', time()))->format('r') }}
			</p>
		@endif
	</div>
	<div class="col-12 d-flex">
		<h3 class="mr-auto">
			License data
		</h3>
		<div class="btn-group mb-2">
			<a href="?download" name="download"
			   class="btn btn-secondary ui-action ui-action-label ui-action-download">
				Download License
			</a>
			<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
			        aria-expanded="false">
				<span class="sr-only">Toggle Dropdown</span>
			</button>
			<div class="dropdown-menu" aria-labelledby="">
				<a class="ui-action-select ui-action-label dropdown-item ui-action" id="activateLink" href="#">
					Activate License
				</a>
				@if ($license->needsReissue())
				<a class="dropdown-item" id="renew" href="?renew">
					<i class="mr-1 fa fa-refresh"></i>
					Renew License
				</a>
				@endif
			</div>
		</div>

	</div>
	<div class="col-12">
		@include('license-data')
	</div>
</div>

<div class="py-1 hide" tabindex="-1" role="dialog" id="activate-license">
	<div class="input">
		<h3 class="mb-3">
			<i class="fa fa-key"></i>
			Activate a license
		</h3>
		<p class="">
			Easily activate a license from <a href="https://my.apiscp.com">my.apiscp.com</a> under
			<b>Settings</b> &gt; <b>Licenses</b>. Upon activation your existing license will be replaced.
			<a href="?download">Download your license</a> before activating.
			If this license is used elsewhere it must first be revoked within my.apnscp.com.
			<br /><br />
			The panel will restart upon successful activation.
		</p>
		<form method="post" action="{{ HTML_Kit::page_url() }}" id="activateForm">
			<label class="d-block">
				Key
				<input type="text" name="key" id="key" class="license-key form-control">
			</label>
			<button type="submit" name="activate" value="1" class="btn btn-primary">
				Activate
			</button>
			<a href="?download" class="btn btn-secondary">
				<i class="ui-action ui-action-download ui-action-label"></i>
				Backup License
			</a>
		</form>
	</div>
</div>