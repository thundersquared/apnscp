$(document).ready(function () {
	draw_chart();
	$('#load_map').click(function () {
		apnscp.call_app(null, 'intialize_map', null).then(function (data) {
			console.log(data);
		});
	});
});

function draw_chart() {
	apnscp.call_app(null, 'get_storage', null, {dataType: "json"}).then(function (data) {
		if (data.length < 1) {
			return false;
		}
		var datapoints = [];
		for (type in data) {
			var items = data[type];
			for (i in items) {
				var item = items[i],
					label = item['label'],
					value = item['used'],
					color = item['color'];
				if (value < 1) {
					continue;
				}
				if (item['extended']) {
					label += ' (' + item['extended'] + ')';
				}
				datapoints.push({label: label, data: value, color: color});
			}
		}

		$.plot('#storagegraph', datapoints, {
			series: {
				pie: {
					clickable: true,
					hoverable: true,
					height: '100px',
					show: true,
					radius: 1,
					combine: {
						threshold: 0.03
					},
					label: {
						background: {opacity: 0.5, color: '#000'},
						show: true,
						radius: 3 / 4,
						formatter: function (label, series) {
							return '<span class="graph-label">' + (series.percent < 1 ? '&lt; ' : '') + Math.round(series.percent) + '%</span>';
						}
					},
					stroke: {
						width: 0
					}
				}
			},
			legend: {
				show: true,
				margin: 0,
				container: $('#storagelegend'),
				labelFormatter: function (name, series) {
					return '<span>' + name + ' &ndash; ' + Math.round(series.percent) + '%</span>';
				}
			}
		});
	});
}