<pre class="" data-output="2-100">
    <code class="language-php">
&lt;?php declare(strict_types=1);
    // replace with a key generated under API Keys
	$key = 'abcdefgh12345';
	$endpoint = '{{ $Page->soapUri() }}';
	$client = new SoapClient(
		$endpoint . '/{{ SOAP_WSDL }}',
		[
			'connection_timeout' => 5,
			'location'           => $endpoint.'/soap?authkey='.$key,
			'uri'                => 'urn:net.apnscp.soap'
		]
    );
	print $client->common_get_uptime();
    </code>
</pre>