<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\setup;

	use apps\setup\models\Authentication;
	use Opcenter\SiteConfiguration;

	class Page extends \Page_Container
	{
		protected $domain;

		public function __construct()
		{
			parent::__construct();
		}

		protected function _layout()
		{
			$this->add_javascript('setup.js');
			parent::_layout();
		}


		public function index() {
			$this->domain = array_get($_GET, 'domain', null);
			if ($this->domain && !preg_match(\Regex::DOMAIN, $this->domain)) {
				warn("Invalid domain");
				$this->domain = null;
			}
			return view('index', [
				'auth' => $this->createAuthModel()
			]);
		}

		public function createAuthModel(): Authentication
		{
			return Authentication::instantiateContexted($this->getAuthContext(),
				[
					SiteConfiguration::shallow($this->getAuthContext()),
					$this->domain
				]);
		}

		public function domains() {
			if ($this->getAuthContext()->level & PRIVILEGE_USER) {
				$domains = (array)array_map(static function ($addr) {
					return substr($addr, strpos($addr, '@')+1);
					}, $this->email_user_mailboxes());
			} else {
				$domains = array_keys($this->web_list_domains());
			}
			return array_unique(
				array_merge(
					[$_SESSION['entry_domain'] ?? $this->getAuthContext()->domain],
					$domains
				)
			);
		}

		public function activeDomain() {
			return $this->domain ?? $this->getAuthContext()->domain;
		}
	}