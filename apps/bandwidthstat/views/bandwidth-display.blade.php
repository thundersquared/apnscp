<div class="center d-flex">
	<h3 class="">Bandwidth Usage</h3>
	@if ($Page->canShowMonth())
		<form method="GET" class="d-block ml-auto">
			<div class="ml-auto btn-group">
				<button type="submit" class="btn @if ($Page->getGrouping() === 'month') btn-outline-primary @else btn-outline-secondary @endif mr-2"
			       name="group" value="month" @if ($Page->getGrouping() === 'month') disabled="DISABLED" @endif>
					Monthly
				</button>
				<button type="submit" name="group" value="day"
		            class="btn @if ($Page->getGrouping() !== 'month') btn-outline-primary @else btn-outline-secondary @endif"
		                @if ($Page->getGrouping() !== 'month') disabled="DISABLED" @endif>
					Daily
				</button>
			</div>
		</form>
	@endif
</div>
<div id="bandwidth-graph" class="mb-3"></div>
@if ($Page->getMode() === 'day' && !$Page->canShowMonth())
	<p class="note mt-1 d-block mb-2">
		Bandwidth predictions will improve on {{ $Page->getMonthChangeOverPoint()->format('F j\<\s\u\p\>S\<\/\s\u\p\>, Y') }}
	</p>
@endif

<h5>Based upon your statistics, the projected bandwidth for this month is</h5>

<table width="100%" class="table">
	<thead>
	<tr>
		<th class="left">Probability</th>
		<th class="right">Range (MB)</th>
		<th class="right">% Utilized</th>
		<th class="right hidden-md-down minor-info">Standard Error</th>
		<th class="right hidden-md-down minor-info">Conf. Interval</th>
		<th class="right hidden-md-down minor-info">t-critical</th>
	</tr>
	</thead>
	@php
		$size = sizeof($bandwidth);
	@endphp
	@foreach ($Page->getIntervalsWithLabels() as $ci => $label)
		@php
			$tstats = $stats['tscores'][$ci];
			$tcrit = $tstats['tcrit'];
			$lower = $tstats['lower'];
			$upper = $tstats['upper'];
			$cssLabel = str_replace(' ', '-', strtolower($label));
		@endphp
		<tr class="{{ $cssLabel }} {{ $color }}">
			<td class="left">{{ $label }}</td>
			<td class="right">
				@if ($lower <= 0)
					&lt;
				@else
					{{ number_format($lower, 2) }} MB &ndash;
				@endif
				{{ number_format($upper, 2) }} MB
			</td>
			<td class="right">

				@if ($lower <= 0)
					&lt;
				@else
					{{ number_format($lower / $bwquota * 100, 2) . '%' }} &ndash;
				@endif
				{{ number_format($upper / $bwquota * 100, 2) . '%' }}
			</td>
			<td class="right hidden-md-down minor-info">
				{{ \Formatter::commafy(sprintf("%.2f MB", $tcrit * $stats['s'] / sqrt($size))) }}
			</td>
			<td class="right hidden-md-down minor-info">{{ printf("%.2f %%", $ci * 100) }}</td>
			<td class="right hidden-md-down minor-info">
				{!! printf("%.2f", $tcrit) !!}
			</td>
		</tr>
		@php
			$color = ($color === 'listodd') ? 'listeven' : 'listodd';
		@endphp
	@endforeach
</table>

<h5><i class="fa fa-calendar"></i> Historic bandwidth data</h5>
<table class="table">
	<thead>
	<tr>
		<th width=150 class="">Date Span</th>
		<th align=center class="hidden-md-down right ">In (MB)</th>
		<th align=center class="hidden-md-down right">Out (MB)</th>
		<th align=center class="right">Aggregate (MB)</th>
		<th align=center class="right">Change (MB)</th>
		<th align=center class="hidden-md-down right">t-score</th>
	</tr>
	</thead>
	<tbody>
	@php
		$previous = array();
		$change = null;
	@endphp
	@for($i = 0, $n = sizeof($bandwidth); $i < $n; $i++)
		@php
			$in = $bandwidth[$i]['in'];
			$out = $bandwidth[$i]['out'];
			$total = $in + $out;
			$begin = date('Y-m-d', $bandwidth[$i]['begin']);
			$end = date('Y-m-d', $bandwidth[$i]['end']);
			if ($i > 0) {
				$change = ($total - $previous['in'] - $previous['out']);
			}
		@endphp
		<tr class="{{ $color }}">
			<td class="left" width=30%">
				<a href="/apps/bandwidthbd?span={{ $bandwidth[$i]['begin'] . ',' . $bandwidth[$i]['end'] }}">
					{{ $begin . ' &ndash; ' . $end }}
				</a>
			</td>
			<td class="hidden-md-down right">
				{{ \Formatter::commafy(sprintf("%.2f MB", $in)) }}
			</td>
			<td class="hidden-md-down right">
				{{ \Formatter::commafy(sprintf("%.2f MB", $out)) }}
			</td>
			<td class="right">
				{{ \Formatter::commafy(sprintf("%.2f MB", $total)) }}
			</td>
			<td class="right @if ($previous) {{ $change > 0 ? 'positive' : 'negative' }} @endif">
				@if ($previous)
					{{ \Formatter::commafy(sprintf("%.2f MB", $change)) }}
					@if ($change > 0)
						<i class="fa fa-caret-up"></i>
					@else
						<i class="fa fa-caret-down"></i>
					@endif
				@else
					&ndash;
				@endif
			</td>
			<td class="right hidden-md-down ">
				@if ($stats['s'] > 0)
					{{ printf("%.2f", ($total - $stats['mean']) / $stats['s'] / sqrt($n)) }}
				@else
					N/A
				@endif
			</td>
		</tr>
		@php
			$color = $color === 'listodd' ? 'listeven' : 'listodd';
			$previous = array(
				'begin' => $begin,
				'end'   => $end,
				'in'    => $in,
				'out'   => $out
			);
		@endphp
	@endfor
	</tbody>
</table>

<h5 class="text-center">
	Additional statistics from calculation
</h5>
<div class="row center">
	<div class="col-4 head1">
		&mu;
	</div>
	<div class="col-4 head1">
		s
	</div>
	<div class="col-4 head1">
		df
	</div>
</div>
<div class="row center">
	<div class="col-4">
		{{ \Formatter::commafy(sprintf("%.2f MB", $stats['mean'])) }}
	</div>

	<div class="col-4">
		{{ \Formatter::commafy(sprintf("%.2f MB", $stats['s'])) }}
	</div>

	<div class="col-4">
		{{ \Formatter::commafy(sprintf("%.2f MB",$stats['df'])) }}
	</div>
</div>
