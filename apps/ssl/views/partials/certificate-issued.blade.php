<h2 class="text-success mb-1 text-left">
	Certificate Detected
</h2>
<hr/>
@php
	$expiryDate = (new DateTime())->setTimestamp($certificate->getX509Field('validTo_time_t'));
	$diff = (new DateTime())->diff($expiryDate);
	$tilexpiry = ($diff->invert ? -1 : 1) * $diff->days;
	$certpair = array(
		'certificate' => 'server.crt',
		'key'         => 'server.key'
	);
	$key = $certificate->getPublicKey();
@endphp
<div id="certificate-server-crt" class="mb-1">
	<h4>Issuer Information</h4>
	<div class="text-truncate mb-3">
		<b>Valid From</b>:
		{{ (new DateTime())->setTimestamp($certificate->getX509Field('validFrom_time_t'))->format('Y/m/d') }}
		to {{ $expiryDate->format('Y/m/d') }}

		@if ($tilexpiry < 0)
			<div class="d-block text-danger alert alert-danger cert-expire-notice error">Your certificate has expired!</div>
		@elseif ($tilexpiry < 30 && !$certificate->isLetsEncrypt())
			{{-- ignore LE certs, these automatically renew in perpetuity --}}
			<div class="d-block cert-expire-notice alert-warning">
				<i class="fa fa-exclamation-triangle"></i> Your certificate will expire in <b>{{ $tilexpiry }}
					days</b>
			</div>
		@endif

		<br/>

		<b>Certificate Fingerprint:</b> {{ $certificate->getX509Field('extensions.subjectKeyIdentifier') }}
		<br/>

		@if ($certificate->isSelfSigned())
			<i class="fa fa-exclamation-triangle text-warning"></i> Certificate is self-signed
			<br/>
		@endif

		@if ($certificate->hasChain())
			<i class="fa fa-check text-info"></i> Certificate requires intermediate certificate
			<br/>
		@endisset

		@if ($certificate->isExtendedValidation())
			<i class="fa fa-check text-success"></i> Certificate is extended validation
			<br/>
		@endif
	</div>

	<div class="mb-3">
		@php
			$url = $certificate->getIssuerUrl();
		@endphp
		<b>Organization:</b>
			{{ $certificate->getIssuerOrganization() }} &ndash;
		@if ($url) <a href="{{ $url }} ">@endif
			{{ $certificate->getIssuerOrganizationUnit() }}
		@if ($url) </a> @endif
		<br/>
		@if (!empty($certificate->getX509Field('issuer.L')))
			<b>Country:</b>
			{{ $certificate->getX509Field('issuer.C') }}
			<br/>
			<b>City:</b>
			{{ $certificate->getX509Field('issuer.L') }}
			<br/>
			<b>State:</b>
			{{ $certificate->getX509Field('issuer.ST') }}
			<br/>
		@endif
		</div>

		<div class="mb-3">
			<b>Key Length:</b>
			{{ $certificate->getKeyField('bits') }}
			<br/>
			<b>Key Type:</b> @if (null !== $certificate->getKeyField('rsa')) RSA @else DSA @endif
		</div>
</div>

<h3>Actions</h3>
<div class="button-actions mb-3">
	@include('partials.certificate-actions')
</div>