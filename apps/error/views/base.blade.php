<div class="row">
	<div class="col-xs-12 text-center mt-3 mx-auto">
		<div class="msg-grab display-1 " id="error-code">
			<?=$Page->getErrorTitle()?>
		</div>
		<div class="scene-wrapper">
			<div id="scene">
				<div id="spaceship">
					<div id="window"></div>
				</div>
				<div id="tail"></div>
				<div id="left-wing"></div>
				<div id="right-wing"></div>
				<div id="exhaust"></div>
				<div id="tail"></div>
			</div>
		</div>
		<div class="msg-grab display-4">
			<?=$Page->getErrorDescription()?>
		</div>
	</div>
</div>
