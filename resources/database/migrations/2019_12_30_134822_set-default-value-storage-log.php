<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetDefaultValueStorageLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		\Illuminate\Support\Facades\DB::connection('pgsql')->getPdo()->exec('ALTER TABLE storage_log ALTER ts 
			SET DEFAULT NOW()::TIMESTAMP(0) WITH TIME ZONE');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
