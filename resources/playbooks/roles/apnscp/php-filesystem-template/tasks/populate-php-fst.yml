- include_vars: "{{ role_path }}/defaults/main.yml"
# Reset php_verison and __php_version. apnscp/initialize-filesystem-template needs to
# occur first, which depends upon php/install completing. apnscp/build-php resets these vars
- set_fact:
    php_version: "{{ system_php_version }}"
    __php_version: >-
      {{ lookup('pipe', php_bin + ' -nr "echo PHP_VERSION;"') }}
  when:
    - not multiphp_build
    - not panel_build
    - php_enabled
- include_vars: "{{ playbook_dir }}/roles/php/build-from-source/vars/main.yml"
  # Avoid clobbering overrides
  when: not multiphp_build | bool
- name: "Add {{ php_dbg }} to php_fst_bins var"
  set_fact: php_fst_bins="{{ php_fst_bins + [php_dbg] }}"
  when: system_php_version is version('7.0', '>=')

- include_role:
    name: php/common
    tasks_from: query-extension-dir

- name: Link PHP config into FST
  include_role: name="common" tasks_from="copy-link.yml"
  vars:
    src: "{{ item }}"
    path: "{{ apnscp_filesystem_template }}/{{service}}{{item}}"
    notify:
      - Drop filesystem caches
      - Reload filesystem template
  with_items: "{{ php_config }}"
- name: Symlink PHP binaries into FST
  file:
    path: "{{ apnscp_filesystem_template }}/{{service}}{{ item }}"
    src: "{{ php_system_install_root }}{{ item }}"
    state: link
    force: yes
  with_items: "{{ php_fst_bins }}"
  when: not multiphp_build

- name: "Validate {{ scan_dir }} exists"
  file:
    path: "{{ scan_dir }}"
    follow: yes
    group: wheel
    mode: g+rwx
    state: directory
- name: "Relocate {{ scan_dir }} into FST"
  copy:
    src: "{{ scan_dir }}"
    dest: "{{ php_fst_prefix }}/{{ scan_dir | dirname}}"
    remote_src: yes
    follow: no
- name: "Stat  {{ scan_dir  }}"
  stat:
    path: "{{ scan_dir }}"
  register: ppst
- name: "Remove {{ scan_dir }}"
  file:
    path: "{{ scan_dir }}"
    state: absent
  when: ppst.stat.exists and ppst.stat.isdir
  # BUG: no clean way to mask /.socket/.../phpXX.d symlink into FST/siteinfo/...
  # /.socket is mounted on top of composite, so symlink always accessible.
  # "mknod siteXX/shadow/.socket/../phpXX.d c 0 0" is the clean way to do this but
  # for each site created we have to reapply this process
  # There's no unintended leakage by keeping an inaccessible symlink in /.socket to FST
- name: "Link socket {{ scan_dir }} into FST"
  file:
    path: "{{ scan_dir }}"
    src: "{{ php_fst_prefix }}{{ scan_dir }}"
    state: link

- name: Add opcache template
  template:
    src: templates/opcache.ini.j2
    dest: "{{ apnscp_filesystem_template }}/{{service}}/{{ scan_dir }}/01-opcache.ini"
    group: wheel
    mode: 0664
  notify: Reload filesystem template

- name: "Relocate extension directory as needed FST{{ php_module_directory }}"
  stat:
    path: "{{ apnscp_filesystem_template }}/{{service}}{{ php_module_directory }}"
  register: st

- name: "Unlink extension directory FST{{ php_module_directory }}"
  file:
    path: "{{ apnscp_filesystem_template }}/{{service}}{{ php_module_directory }}"
    state: absent
  when: st.stat.exists and st.stat.isdir

- name: "Validate module source directory {{ php_module_directory }}"
  stat:
    path: "{{ php_module_directory }}"
  register: st

- name: Make extension directory
  file:
    path: "{{ apnscp_filesystem_template }}/{{ service }}{{ php_module_directory }}"
    state: "link"
    src: "{{ php_prefix}}{{ php_module_directory }}"
  when: multiphp_build | bool
- name: "Check {{php_module_directory }} type"
  stat:
    path: "{{ php_module_directory }}"
  register: st
  when: multiphp_build | bool
- name: Remove multiPHP extension directory if exists
  file:
    path: "{{ php_module_directory }}"
    state: absent
  when: multiphp_build | bool and st.stat.exists and st.stat.isdir
  # Links FST/siteinfo/usr/lib64xxyy to /usr/lib64/xxyy
- name: Link system extension directory to socket
  file:
    path: "{{ php_fst_prefix }}{{ php_module_directory }}"
    src: "{{ php_system_install_root + php_module_directory }}"
    state: link
  when: not multiphp_build | bool
  # ini config points to /usr/lib64/API, but installs to apnscp_shared_root/..
  # let's fix that
- name: Link multiPHP extension directory to socket
  file:
    path: "{{ php_module_directory }}"
    src: "{{ multiphp_php_prefix + php_module_directory }}"
    state: link
  when: multiphp_build | bool and (not st.stat.exists or st.stat.isdir)

- name: Add opcache extension
  copy:
    src: "{{ php_module_directory }}/opcache.so"
    dest: "{{ php_fst_prefix }}/{{ php_module_directory }}/opcache.so"
    remote_src: yes
  notify: Reload filesystem template
  when: not multiphp_build | bool

- name: Add ionCube loader
  include_role: name=php/build-from-source tasks_from=install-ioncube.yml
  vars:
    php_prefix: "{{ php_fst_prefix }}"
    phpconfig: "{{ php_fst_prefix }}/bin/php-config"
    # Force reset of php_config_path
    notify:
      - "{{ php_build_fpm | default(not has_dns_only) | ternary('Restart php-fpm', 'Restart apache') }}"
      - Reload filesystem template
  when:
    # Assigning a default in the when condition updates vars globally?
    # What a peculiar beast Ansible is...
    - php_install_ioncube | default(False) | bool
    - (__php_version | default(system_php_version) | regex_replace('^(\\d+)\\.(\\d+).*$', '\\1.\\2')) is version('8.0', '!=')
    - __php_version | default(system_php_version) is version('8.2.0', '<')

- name: Add SourceGuardian loader
  include_role: name=php/build-from-source tasks_from=install-sourceguardian.yml
  vars:
    php_prefix: "{{ php_fst_prefix }}"
    phpconfig: "{{ php_fst_prefix }}/bin/php-config"
    # Force reset of php_config_path
    notify:
      - "{{ php_build_fpm | default(not has_dns_only) | ternary('Restart php-fpm', 'Restart apache') }}"
      - Reload filesystem template
  when:
    - php_install_sourceguardian | default(False) | bool
    - __php_version | default(system_php_version) is version('8.2.0', '<')

- meta: flush_handlers
