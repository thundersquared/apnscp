<h5>Activity Information</h5>
IP address: {{ $ip ?? "UNKNOWN" }} ({{ $hostname ?? "UNKNOWN" }}) <br/>
@if (!empty($latitude))
    {{ $city }}, {{ $state }} ({{ $country }}) <br/>
    [{{ $latitude }}&deg;, {{ $longitude }}&deg;] - <a href="{{ $gmapurl }}">Google Maps</a> <br/><br/>
@endif
@if (!empty($browser))
    <br />
    Browser: {{ $browser }}<br/>
@endif
