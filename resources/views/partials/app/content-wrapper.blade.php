<div class="container-fluid ui-app-row p-0">
	@includeWhen(Page_Renderer::do_nav(), 'theme::partials.user.nav')
	@includeWhen(Page_Renderer::do_header(), 'theme::partials.user.notification-sink')
	@include('theme::partials.app.content-loading')
	@php
		while (ob_get_level() > 1) {
			ob_end_flush();
		}
		ob_get_level() && ob_flush();
		flush();
	@endphp
	@include('theme::partials.app.content')
</div>