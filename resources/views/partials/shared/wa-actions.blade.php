<div class="btn-group {{ $btnGroupClass ?? '' }}">
	<a class="ui-action ui-action-select ui-action-label btn btn-secondary" href="{{ $href }}">
		Select
	</a>
	<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
	        aria-haspopup="true"
	        aria-expanded="false">
		<span class="sr-only">Toggle Dropdown</span>
	</button>
	<div class="dropdown-menu {{ $dropdownLocation ?? 'dropdown-menu-left' }}" aria-labelledby="">
		@if (!$pane->getPath() && $pane->getHostname() !== \Session::get('domain'))
			<a class="fa-globe ui-action-label dropdown-item ui-action"
			   href="{{ $pane->addonTypeMangementLink() }}">
				Manage @if ($pane->isSubdomain()) Subdomain @else Domain @endif
			</a>
		@endif
		<a class="ui-action-visit-site ui-action-label dropdown-item ui-action"
		   href="{{ $pane->getUrl() }}">
			Visit Site
		</a>
		<a class="ui-action-manage-files ui-action-label dropdown-item ui-action"
		   href="{{ HTML_Kit::new_page_url_params('/apps/filemanager', ['f' => $pane->getDocumentRoot()]) }}">
			Manage Files
		</a>
		<a class="fa-apache ui-action-label dropdown-item ui-action"
		   href="{{ HTML_Kit::new_page_url_params('/apps/personalities', $pane->toArray())}}">
			Edit .htaccess
		</a>

		<a class="fa-php-w ui-action-label dropdown-item ui-action"
		   href="{{ HTML_Kit::new_page_url_params('/apps/php-editor', $pane->toArray())}}">
			Edit PHP Settings
		</a>

		@if ($pane->getMeta('failed'))
			<div class="dropdown-divider"></div>
			<a class="ui-action fa-check dropdown-item ui-action-label ui-action-warn"
			   data-toggle="tooltip"
			   data-title="Clear FAILED status. Enroll site in 1-click updates."
			   href="{{  HTML_Kit::new_page_url_params('/apps/webapps', ['clear' => $pane->getDocumentRoot()]) }}">
				Reset Failed
			</a>
		@endif

	</div>

</div>