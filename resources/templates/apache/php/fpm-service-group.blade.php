# via {{ $templatePath }}
[Unit]
Description=PHP worker for {!! $config->getGroup() !!}
@if (version_compare(os_version(), '8', '<'))
DefaultDependencies=no
Requires=sysinit.target
@endif
After=network.target php-fpm.service
PartOf=php-fpm.service
RequiresMountsFor={!! $config->getRoot() !!}

[Service]
ExecStop=/bin/true
Type=oneshot
RemainAfterExit=yes

[Install]
WantedBy=php-fpm.service
